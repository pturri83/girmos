"""Program to simulate the analytic calculation of PSF parameters during GIRMOS
operations.

References:
    [ref1] Ge et al., 1997
    [ref2] Sarazin & Roddier, 1990
    [ref3] Kolb, 2008
"""


import glob
from os import path
import pickle
import re
import time

import numpy as np

from source import params_girmos, psf


# Data parameters
folder = '/Volumes/Astro/Data/GIRMOS/Simulations'  # WFR folder
sim_n = 1  # Simulation number
r_0 = 0.186  # Fried parameter (m)

# Prepare files
print("\nProgram started")
start_time = time.time()
folder = path.join(folder, ('sim_' + str(sim_n)))
files = glob.glob(path.join(folder, 'wfr', 'n_*_x_*_y_*.mat'))
n_files = len(files)

# Use different wavelengths and NCPAs
wl_names_ok = np.array(params_girmos.wl_names)
wls_ok = np.array(params_girmos.wls)
fwhm_co = psf.co_fwhm()

for wl_name in wl_names_ok:

    # Choose wavelength
    print("- {0} band".format(wl_name))
    wl = wls_ok[np.where(np.array(wl_names_ok) == wl_name)[0][0]] * 1e-6
    folder_out = path.join(folder, 'psf', wl_name)

    # Prepare output
    filename = []
    x = np.zeros(n_files)
    y = np.zeros(n_files)
    zenith = np.zeros(n_files)
    # azimuth = np.zeros(n_files)
    sr = np.zeros(n_files)
    sr_marechal = np.zeros(n_files)
    fwhm = np.zeros(n_files)
    fwhm_ell = np.zeros(n_files)
    fwhm_pa = np.zeros(n_files)
    enc0025 = np.zeros(n_files)
    enc005 = np.zeros(n_files)
    enc01 = np.zeros(n_files)
    rho = np.arange(0, 1e-6, 1e-9)
    i_rho = np.zeros([n_files, len(rho)])

    # Process WFRs
    for i_file in range(n_files):

        # Calculate metrics
        print("\r      - Calculating PSF # {0}/{1} ...".format(
            (i_file + 1), n_files), end="")
        file = files[i_file]
        filename.append(path.split(file)[1])
        file_split = re.split('n_|_x_|_y_|.mat', file)
        x[i_file] = float(file_split[2])
        y[i_file] = float(file_split[3])
        zenith[i_file] = np.hypot(x[i_file], y[i_file])
        wfr = psf.wfr_load(file)
        sigma, sigma_tt, sigma_tt_dec, sigma_ho = psf.wfr_std(wfr)
        sigma_ho *= 2 * np.pi / wl
        i_c = (np.exp(-(sigma_ho ** 2))) / \
            (1 + (1.49 * ((psf.d * sigma_tt / wl) ** 2)))  # [ref1], Eq. (17)
        i_h = (1 - np.exp(-(sigma_ho ** 2))) / (1 + ((psf.d / r_0) ** 2))
        # [ref1], Eq. (18)
        sr[i_file] = i_c + i_h  # [ref1], Eq. (20)
        sr_marechal[i_file] = np.exp(-((2 * np.pi * sigma / wl) ** 2))
        w_c = np.sqrt(((1.028 * wl * fwhm_co / psf.d) ** 2) +
                      ((2 * np.sqrt(2 * np.log(2)) * sigma_tt) ** 2))  # [ref1],
        # Eq. (15) for a circular aperture with obstructions; 1.22 is incorrect,
        # it has been substituted with 1.028
        w_h = 0.9759 * wl / r_0  # [ref1], Eq. (16); 1.22 is incorrect, it has
        # been substituted with 0.9759 [ref2,3]
        fwhm[i_file] = psf.fwhm_double_gauss(i_c, w_c, i_h, w_h)
        fwhm_ell[i_file] = (sigma_tt_dec[0] - sigma_tt_dec[1]) / sigma_tt_dec[0]
        fwhm_pa[i_file] = sigma_tt_dec[2]
        enc0025[i_file] = (1 / (((w_c ** 2) * i_c) + ((w_h ** 2) * i_h))) * \
                          (((w_c ** 2) * i_c *
                            (1 - np.exp(-4 * np.log(2) *
                                        ((0.025 / params_girmos.rad_to_arcsec)
                                         ** 2) /
                                        (w_c ** 2)))) +
                           ((w_h ** 2) * i_h *
                            (1 - np.exp(-4 * np.log(2) *
                                        ((0.025 / params_girmos.rad_to_arcsec)
                                         ** 2) /
                                        (w_h ** 2)))))  # [ref1], Eq. (21)
        enc005[i_file] = (1 / (((w_c ** 2) * i_c) + ((w_h ** 2) * i_h))) * \
                         (((w_c ** 2) * i_c *
                           (1 - np.exp(-4 * np.log(2) *
                                       ((0.05 / params_girmos.rad_to_arcsec)
                                        ** 2) /
                                       (w_c ** 2)))) +
                          ((w_h ** 2) * i_h *
                           (1 - np.exp(-4 * np.log(2) *
                                       ((0.05 / params_girmos.rad_to_arcsec)
                                        ** 2) /
                                       (w_h ** 2)))))  # [ref1], Eq. (21)
        enc01[i_file] = (1 / (((w_c ** 2) * i_c) + ((w_h ** 2) * i_h))) * \
                        (((w_c ** 2) * i_c *
                          (1 - np.exp(-4 * np.log(2) *
                                      ((0.1 / params_girmos.rad_to_arcsec)
                                       ** 2) /
                                      (w_c ** 2)))) +
                         ((w_h ** 2) * i_h *
                          (1 - np.exp(-4 * np.log(2) *
                                      ((0.1 / params_girmos.rad_to_arcsec)
                                       ** 2) /
                                      (w_h ** 2)))))  # [ref1], Eq. (21)

        # Calculate profile
        i_rho[i_file, :] = \
            (i_c * np.exp(-4 * np.log(2) * ((rho / w_c) ** 2))) + \
            (i_h * np.exp(-4 * np.log(2) * ((rho / w_h) ** 2)))

    fwhm *= params_girmos.rad_to_arcsec
    rho *= params_girmos.rad_to_arcsec

    # Build the grid
    # x = zenith * np.cos(np.deg2rad(azimuth))
    # y = zenith * np.sin(np.deg2rad(azimuth))
    # x = np.around(x, decimals=10)
    # y = np.around(y, decimals=10)
    n_side = int(np.sqrt(n_files))
    x_sort = sorted(x)
    max_range = int(np.mean(x_sort[-n_side:]))
    steps = np.linspace(-max_range, max_range, num=n_side)
    col = np.zeros(n_files)
    row = np.zeros(n_files)

    for i_row in range(n_side):
        for i_col in range(n_side):
            x_cell = steps[i_col]
            y_cell = steps[i_row]
            dr = np.hypot((x - x_cell), (y - y_cell))
            idx = np.argmin(dr)
            col[idx] = i_col
            row[idx] = i_row

    # Save output
    print("\n      - Saving results ...")
    metrs = {'wl_name': wl_name, 'wl': wl, 'n': n_files, 'file': filename,
             'x': x, 'y': y, 'zenith': zenith, 'col': col, 'row': row,
             'max_range': max_range, 'sr': sr, 'sr_marechal': sr_marechal,
             'fwhm': fwhm, 'fwhm_ell': fwhm_ell, 'fwhm_pa': fwhm_pa,
             'enc0025': enc0025, 'enc005': enc005, 'enc01': enc01, 'rho': rho,
             'i_rho': i_rho}
    file_out_metrs = open(path.join(folder_out, 'metrics_operation_0.pkl'),
                          'wb+')
    pickle.dump(metrs, file_out_metrs)
    file_out_metrs.close()

# End program
end_time = time.time()
elapsed_time = end_time - start_time
elapsed_h = int(np.floor(elapsed_time / (60 * 60)))
elapsed_m = int(np.floor((elapsed_time / 60) - (elapsed_h * 60)))
elapsed_s = int(np.floor(elapsed_time - (elapsed_m * 60) -
                (elapsed_h * 60 * 60)))
print("Time elapsed: {0:02d}h:{1:02d}m:{2:02d}s".format(elapsed_h, elapsed_m,
                                                        elapsed_s))
print("Program terminated\n")
