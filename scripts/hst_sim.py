"""Program to simulate a GIRMOS science image of a z~2 galaxy from an H_alpha
mosaic image by HST of M51 and the GIRMOS PSF grid (produced with 'psf_sim.py').
"""


from os import path
import pickle
import time

from astropy.io import fits
from matplotlib import pyplot as plt
import numpy as np
import scipy.constants

from source import cube_girmos, misc_girmos, psf


# Data parameters
psf_folder = '/Volumes/Astro/Data/GIRMOS/PSF/glao_moao'  # Input PSF folder
wl_name = 'H'  # 2MASS filter ('J', 'H', 'Ks')
ncpa = 0  # NCPA RMS (nm)
science_folder = '/Volumes/Astro/Data/GIRMOS/Science/HST'  # Science folder
science_img = 'h_m51_h_s05_drz_sci'  # Input science image

# Simulation parameters
z = 2  # Redhsift
ps_in = 0.05  # Input science image pixel scale ("/px)
ps_girmos = 0.025  # GIRMOS spaxel scale ("/px)
fov_girmos = 1  # GIRMOS FOV (")
dist = 8.58  # M51 distance (Mpc) (McQuinn et al. 2016)
direct_gal = [-0.1, -0.5]  # Direction of the IFU respect to the galaxy ([x, y])
# (")
direct_ifu = [50, 0]  # Direction of the IFU respect to the GIRMOS optical axis
# ([x, y]) (")
psf_r = 0.8  # Radius of the PSF used for the convolution (")

# Graphic parameters
range_val = [0, 0.1]  # Range of values shown ([min, max])
ticks_in = 100  # Distance of the axes' ticks in the input image (")
ticks_z = 1  # Distance of the axes' ticks in the redshifted image (")
ticks_out = 0.5  # Distance of the axes' ticks in the output image (")

# Load the PSF
print("\nProgram started")
start_time = time.time()
print("Loading data ...")
folder = path.join(psf_folder, wl_name)
file_in = open(path.join(folder, 'psfs_over_{0}.pkl'.format(str(ncpa))), 'rb')
psf_in = pickle.load(file_in)
file_in.close()
psf_ps = psf_in['ps']

# Select the PSF
psf_dir = psf.interpolate_dir(psf_in, direct_ifu)

# Show the PSF
psf.psf_plot(psf_dir, psf_ps, zoom=1, ticks=0.5,
             title="Oversampled PSF ({0})".format(wl_name), close=False)
psf.psf_rad(psf_dir, psf_in['psf_where'], psf_ps, zoom=psf_r)

# Cut the PSF
psf_center = np.where(psf_dir == np.max(psf_dir))
psf_center_x = psf_center[1][0]
psf_center_y = psf_center[0][0]
psf_x = np.linspace(0, (psf_dir.shape[1] - 1), psf_dir.shape[1]) - psf_center_x
psf_y = np.linspace(0, (psf_dir.shape[0] - 1), psf_dir.shape[0]) - psf_center_y
psf_mesh_x, psf_mesh_y = np.meshgrid(psf_x, psf_y)
psf_r_px = int(psf_r / psf_ps)
psf_where = np.where(np.hypot(psf_mesh_x, psf_mesh_y) > psf_r_px)
psf_dir[psf_where] = 0
psf_cut = psf_dir[(psf_center_y - psf_r_px): (psf_center_y + psf_r_px),
                  (psf_center_x - psf_r_px): (psf_center_x + psf_r_px)]

# Load the image
image = fits.getdata(path.join(science_folder, (science_img + '.fits')), ext=0)
hdul = fits.open(path.join(science_folder, (science_img + '.fits')))
hdr = hdul[0].header
hdul.close()

# Show the original image
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(image.shape, ps_in, ticks_in)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(image, cmap='gray', vmin=range_val[0], vmax=range_val[1],
           origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title("Input image")

# Move the image to redshift z
ps_pc = dist * 1e6 * np.tan(np.deg2rad(ps_in / 3600))  # Input science image
# pixel scale (pc/px)
h_0 = 70  # Local Hubble constant (km/s/Mpc)
ps_z = np.rad2deg((h_0 * 1e-3 * ps_pc * ((1 + z)**(3 / 2))) /
                  (2 * scipy.constants.c * (((1 + z)**(1 / 2)) - 1))) * 3600
# Image pixel scale at redshift z ("/px)

# Show the moved image
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(image.shape, ps_z, ticks_z)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(image, cmap='gray', vmin=range_val[0], vmax=range_val[1],
           origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title("\"Redshifted\" (z={0}) image".format(z))

# Zoom the image to fit the GIRMOS FOV
image_size = np.array(image.shape)
image_center = image_size / 2
image_center += np.flip(direct_gal) / ps_z
image_center = image_center.astype(int)
image_half_side = int(np.ceil(((fov_girmos / 2) + psf_r + ps_girmos) / ps_z))

if np.any((image_center - image_half_side) < 0) or \
        np.any((image_center + image_half_side) > image_size):
    raise Exception("The value of 'psf_r' is too large for the size of the" +
                    "image, try to reduce it")

image_fov = image[(image_center[0] - image_half_side):
                  (image_center[0] + image_half_side),
                  (image_center[1] - image_half_side):
                  (image_center[1] + image_half_side)]

# Show the zoomed image
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(image_fov.shape, ps_z, ticks_out)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(image_fov, cmap='gray', vmin=range_val[0], vmax=range_val[1],
           origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title("Zoomed image")

# Scale the image to fit the PSF
image_scale = cube_girmos.scale(image_fov, ps_z, psf_ps)

# Show the scaled image
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(image_scale.shape, psf_ps, ticks_out)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(image_scale, cmap='gray', vmin=range_val[0], vmax=range_val[1],
           origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title("Scaled image")

# Convolve the image with the GIRMOS PSF
image_conv = cube_girmos.convolve(image_scale, psf_cut)

# Show the convolved image
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(image_conv.shape, psf_ps, ticks_out)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(image_conv, cmap='gray', vmin=range_val[0], vmax=range_val[1],
           origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title("Convolved image ({0})".format(wl_name))

# Scale the image to GIRMOS
image_scale_girmos = cube_girmos.scale(image_conv, psf_ps, ps_girmos)

# Show the scaled to GIRMOS image
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(image_scale_girmos.shape, ps_girmos, ticks_out)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(image_scale_girmos, cmap='gray', vmin=range_val[0],
           vmax=range_val[1], origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title("Image scaled to GIRMOS ({0})".format(wl_name))

# Cut the image to the GIRMOS FOV
image_cut_size = image_scale_girmos.shape[0]
image_cut_0 = int((image_cut_size - (fov_girmos / ps_girmos)) / 2)
image_cut_1 = image_cut_0 + int(fov_girmos / ps_girmos)
image_out = image_scale_girmos[image_cut_0: image_cut_1,
                               image_cut_0: image_cut_1]

# Show the image cut to the GIRMOS FOV
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(image_out.shape, ps_girmos, ticks_out)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(image_out, cmap='gray', vmin=range_val[0], vmax=range_val[1],
           origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title("Image simulated with GIRMOS ({0})".format(wl_name))
end_time = time.time()
elapsed_time = end_time - start_time
elapsed_m = int(np.floor(elapsed_time / 60))
elapsed_s = int(np.floor(elapsed_time) - (elapsed_m * 60))
print("Time elapsed: {0:02d}m:{1:02d}s".format(elapsed_m, elapsed_s))
print("Program terminated\n")
