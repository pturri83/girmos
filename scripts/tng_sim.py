"""Program to simulate a GIRMOS science image of a z~2 galaxy from an H_alpha
map from the IllustrisTNG TNG100-1 simulation and the GIRMOS PSF grid (produced
with 'psf_sim.py').

References:
    [ref1] Condon et Matthews, 2018
    [ref2] Allen, Astrophysical Quantities
"""


import h5py
from os import path
import pickle
import time

from matplotlib import pyplot as plt
import numpy as np
import scipy.constants

from source import cube_girmos, cube_tng, misc_girmos, psf


# Data parameters
psf_folder = '/Volumes/Astro/Data/GIRMOS/PSF/glao_moao'  # Input PSF folder
wl_name = 'H'  # 2MASS filter ('J', 'H', 'Ks')
ncpa = 0  # NCPA RMS (nm)
science_folder = '/Volumes/Astro/Data/GIRMOS/Science/TNG'  # Science folder
science_data = 'cutout_10756'  # Input science data

# Simulation parameters
flat_axis = 'z'  # Select the simulation coordinate axis to flatten for the
# projection ('x', 'y' or 'z')
ps_girmos = 0.025  # GIRMOS spaxel scale ("/px)
fov_girmos = 1  # GIRMOS FOV (")
direct_ifu = [50, 0]  # Direction of the IFU respect to the GIRMOS optical axis
# ([x, y]) (")
psf_r = 0.8  # Radius of the PSF used for the convolution (")

# Graphic parameters
ticks_out = 0.5  # Distance of the axes' ticks in the output image (")

# Fixed parameters
d1 = 8  # Gemini Telescope outer diameter (m)
d2 = 1  # Gemini Telescope outer diameter (m)

# Load the PSF
print("\nProgram started")
start_time = time.time()
print("Loading data ...")
folder = path.join(psf_folder, wl_name)
file_in = open(path.join(folder, 'psfs_over_{0}.pkl'.format(str(ncpa))), 'rb')
psf_in = pickle.load(file_in)
file_in.close()
psf_ps = psf_in['ps']

# Select the PSF
psf_dir = psf.interpolate_dir(psf_in, direct_ifu)

# Show the PSF
psf.psf_plot(psf_dir, psf_in['ps'], zoom=1, ticks=0.5, title="Oversampled PSF",
             close=False)
psf.psf_rad(psf_dir, psf_in['psf_where'], psf_in['ps'], zoom=psf_r)

# Cut the PSF
psf_center = np.where(psf_dir == np.max(psf_dir))
psf_center_x = psf_center[1][0]
psf_center_y = psf_center[0][0]
psf_x = np.linspace(0, (psf_dir.shape[1] - 1), psf_dir.shape[1]) - psf_center_x
psf_y = np.linspace(0, (psf_dir.shape[0] - 1), psf_dir.shape[0]) - psf_center_y
psf_mesh_x, psf_mesh_y = np.meshgrid(psf_x, psf_y)
psf_r_px = int(psf_r / psf_in['ps'])
psf_where = np.where(np.hypot(psf_mesh_x, psf_mesh_y) > psf_r_px)
psf_dir[psf_where] = 0
psf_cut = psf_dir[(psf_center_y - psf_r_px): (psf_center_y + psf_r_px),
                  (psf_center_x - psf_r_px): (psf_center_x + psf_r_px)]

# Load the data
gal_hdf5 = h5py.File(path.join(science_folder, (science_data + '.hdf5')), 'r')
sfr = gal_hdf5['PartType0']['StarFormationRate']  # M_sun/yr
sfr_coord = gal_hdf5['PartType0']['Coordinates']  # ckpc/h
header = dict(gal_hdf5['Header'].attrs.items())
z = header['Redshift']
h = header['HubbleParam']

# Convert coordinates
a = 1 / (1 + z)  # [ref1(36)]
h_0 = 100 * h  # km/s/Mpc [ref1(10)]
d_h_0 = (scipy.constants.c / h_0) * 1e-3  # Mpc [ref1(13)]
d_c = d_h_0 / ((a / (1 - a)) + 0.2278 + (0.207 * (1 - a) / (0.785 + a)) -
               (0.0158 * (1 - a) / ((0.312 + a) ** 2)))  # cMpc [ref1(36)]
theta = (1 + z) / d_c  # rad/cMpc [ref1(43)]
arcsec = theta * h * 1e-3 * 60 * 60 * 180 / np.pi  # "/rad ckpc/cMpc
sfr_coord_x = sfr_coord[:, 0] * arcsec  # "
sfr_coord_y = sfr_coord[:, 1] * arcsec  # "
sfr_coord_z = sfr_coord[:, 2] * arcsec  # "

if flat_axis == 'x':
    sfr_coord_1 = sfr_coord_y
    sfr_coord_2 = sfr_coord_z
elif flat_axis == 'y':
    sfr_coord_1 = sfr_coord_x
    sfr_coord_2 = sfr_coord_z
else:
    sfr_coord_1 = sfr_coord_x
    sfr_coord_2 = sfr_coord_y

# Calculate simulation
halpha_lum, gas_part = cube_tng.h_alpha(sfr, sfr_coord_1, sfr_coord_2,
                                        psf_ps)  # erg/s

# Cut simulation
map_center = np.where(halpha_lum == np.max(halpha_lum))
fov_sim = int(1.5 * fov_girmos / psf_ps)
halpha_lum = halpha_lum[(map_center[0][0] - fov_sim):
                        (map_center[0][0] + fov_sim),
                        (map_center[1][0] - fov_sim):
                        (map_center[1][0] + fov_sim)]
gas_part = gas_part[(map_center[0][0] - fov_sim): (map_center[0][0] + fov_sim),
                    (map_center[1][0] - fov_sim): (map_center[1][0] + fov_sim)]

# Calculate image
d_l = (1 + z) * d_c  # Mpc [ref1(52)]
halpha_flux1 = halpha_lum / (4 * np.pi * (d_l ** 2))  # erg/s/Mpc^2 [ref1(51)]
meter = (3.085678e16 * 1e6) ** 2  # m^2/Mpc^2 [ref2]
halpha_flux2 = halpha_flux1 / meter  # erg/s/m^2
a1 = np.pi * ((d1 / 2) ** 2)  # m^2
a2 = np.pi * ((d2 / 2) ** 2)  # m^2
a = a1 - a2  # m^2
halpha_int = halpha_flux2 * a  # erg/s

# Show the original image
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(halpha_int.shape, psf_ps, ticks_out)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(halpha_int, origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title(r"$H_{\alpha}$ original image")

# Show the particles
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(gas_part.shape, psf_ps, ticks_out)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(gas_part, origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title("Gas particles")

# Convolve the image with the GIRMOS PSF
halpha_conv = cube_girmos.convolve(halpha_int, psf_cut)

# Show the convolved image
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(halpha_conv.shape, psf_ps, ticks_out)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(halpha_conv, cmap='gray', origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title(r"$H_{\alpha}$ convolved image")

# Scale the image to GIRMOS
halpha_girmos = cube_girmos.scale(halpha_conv, psf_ps, ps_girmos)

# Show the scaled to GIRMOS image
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(halpha_girmos.shape, ps_girmos, ticks_out)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(halpha_girmos, cmap='gray', origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title(r"$H_{\alpha}$ image scaled to GIRMOS")

# Cut the image to the GIRMOS FOV
halpha_cut_size = halpha_girmos.shape[0]
halpha_cut_0 = int((halpha_cut_size - (fov_girmos / ps_girmos)) / 2)
halpha_cut_1 = halpha_cut_0 + int(fov_girmos / ps_girmos)
halpha_out = halpha_girmos[halpha_cut_0: halpha_cut_1,
                           halpha_cut_0: halpha_cut_1]

# Show the image cut to the GIRMOS FOV
x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
    misc_girmos.ticks_scale(halpha_out.shape, ps_girmos, ticks_out)
fig = plt.figure(figsize=(5, 5))
ax = fig.add_subplot(111)
plt.imshow(halpha_out, cmap='gray', origin='lower')
x_lim = plt.xlim()
y_lim = plt.ylim()
ax.set_xlabel("x (\")")
ax.set_ylabel("y (\")")
ax.set_xticks(x_ticks_orig)
ax.set_yticks(y_ticks_orig)
ax.set_xticklabels(x_ticks_str)
ax.set_yticklabels(y_ticks_str)
ax.set_xlim(x_lim)
ax.set_ylim(y_lim)
plt.title(r"$H_{\alpha}$ image simulated with GIRMOS")
end_time = time.time()
elapsed_time = end_time - start_time
elapsed_m = int(np.floor(elapsed_time / 60))
elapsed_s = int(np.floor(elapsed_time) - (elapsed_m * 60))
print("Time elapsed: {0:02d}m:{1:02d}s".format(elapsed_m, elapsed_s))
print("Program terminated\n")
