"""Program to plot the results of the analytic calculation of PSF parameters
during GIRMOS operations.
"""


from os import makedirs, path
import pickle
import shutil

import numpy as np

from source import params_girmos, psf


# Data parameters
folder = '/Volumes/Astro/Data/GIRMOS/Simulations/sim_1'  # WFR folder

# Analysis parameters
fwhm_sim = 'geometric'#'arithmetic'  # Simulated FWHM to analyze ('arithmetic' or
# 'geometric')

# Graphic parameters
tick_step = 20  # Axes' tick step (")

# Use different wavelengths and NCPAs
print("\nProgram started")
wl_names = np.array(params_girmos.wl_names)
wls_arr = np.array(params_girmos.wls)
fwhm_co = psf.co_fwhm()

for wl_name in wl_names:

    # Load metrics
    print("- {0} band".format(wl_name))
    wl = wls_arr[np.where(np.array(wl_names) == wl_name)[0][0]] * 1e-6
    print("   - Loading data ...")
    folder_in = path.join(folder, 'psf', wl_name)
    file_psf = open(path.join(folder_in, 'metrics_0.pkl'), 'rb')
    metr_psf = pickle.load(file_psf)
    file_psf.close()
    file_psf_i = open(path.join(folder_in, 'psfs_over_0.pkl'), 'rb')
    psf_i = pickle.load(file_psf_i)
    file_psf_i.close()
    file_oper = open(path.join(folder_in, 'metrics_operation_0.pkl'), 'rb')
    metr_oper = pickle.load(file_oper)
    file_oper.close()
    n = metr_psf['n']
    col_list_psf = metr_psf['col']
    col_list_oper = metr_oper['col']
    row_list_psf = metr_psf['row']
    row_list_oper = metr_oper['row']
    zenith_list_psf = metr_psf['zenith']
    sr_list_psf = metr_psf['sr']
    sr_list_oper = metr_oper['sr']
    sr_list_marechal = metr_oper['sr_marechal']
    fwhm_a_list_psf = metr_psf['fwhm_a']#####remove
    fwhm_g_list_psf = metr_psf['fwhm_g']####rename
    fwhm_list_oper = metr_oper['fwhm']
    fwhm_ell_list_psf = metr_psf['fwhm_ell']
    fwhm_ell_list_oper = metr_oper['fwhm_ell']
    fwhm_pa_list_psf = metr_psf['fwhm_pa']
    fwhm_pa_list_oper = metr_oper['fwhm_pa']
    enc0025_list_psf = metr_psf['enc0025']
    enc0025_list_oper = metr_oper['enc0025']
    enc005_list_psf = metr_psf['enc005']
    enc005_list_oper = metr_oper['enc005']
    enc01_list_psf = metr_psf['enc01']
    enc01_list_oper = metr_oper['enc01']
    rho_oper = metr_oper['rho']
    i_rho_oper = metr_oper['i_rho']

    # Position PSF parameters
    n_side = int(np.sqrt(n))
    max_range = metr_psf['max_range']
    zenith_arr = np.zeros((n_side, n_side))
    sr_arr_psf = np.zeros((n_side, n_side))
    sr_arr_oper = np.zeros((n_side, n_side))
    sr_arr_marechal = np.zeros((n_side, n_side))
    fwhm_a_arr_psf = np.zeros((n_side, n_side))
    fwhm_g_arr_psf = np.zeros((n_side, n_side))
    fwhm_arr_oper = np.zeros((n_side, n_side))
    fwhm_ell_arr_psf = np.zeros((n_side, n_side))
    fwhm_ell_arr_oper = np.zeros((n_side, n_side))
    fwhm_pa_arr_psf = np.zeros((n_side, n_side))
    fwhm_pa_arr_oper = np.zeros((n_side, n_side))
    enc0025_arr_psf = np.zeros((n_side, n_side))
    enc0025_arr_oper = np.zeros((n_side, n_side))
    enc005_arr_psf = np.zeros((n_side, n_side))
    enc005_arr_oper = np.zeros((n_side, n_side))
    enc01_arr_psf = np.zeros((n_side, n_side))
    enc01_arr_oper = np.zeros((n_side, n_side))

    for idx in range(n):
        i_col_psf = int(col_list_psf[idx])
        i_col_oper = int(col_list_oper[idx])
        i_row_psf = int(row_list_psf[idx])
        i_row_oper = int(row_list_oper[idx])
        zenith_arr[i_row_psf, i_col_psf] = zenith_list_psf[idx]
        sr_arr_psf[i_row_psf, i_col_psf] = sr_list_psf[idx]
        sr_arr_oper[i_row_oper, i_col_oper] = sr_list_oper[idx]
        sr_arr_marechal[i_row_oper, i_col_oper] = sr_list_marechal[idx]
        fwhm_a_arr_psf[i_row_psf, i_col_psf] = fwhm_a_list_psf[idx]
        fwhm_g_arr_psf[i_row_psf, i_col_psf] = fwhm_g_list_psf[idx]
        fwhm_arr_oper[i_row_oper, i_col_oper] = fwhm_list_oper[idx]
        fwhm_ell_arr_psf[i_row_psf, i_col_psf] = fwhm_ell_list_psf[idx]
        fwhm_ell_arr_oper[i_row_psf, i_col_psf] = fwhm_ell_list_oper[idx]
        fwhm_pa_arr_psf[i_row_psf, i_col_psf] = fwhm_pa_list_psf[idx]
        fwhm_pa_arr_oper[i_row_psf, i_col_psf] = fwhm_pa_list_oper[idx]
        enc0025_arr_psf[i_row_psf, i_col_psf] = enc0025_list_psf[idx]
        enc0025_arr_oper[i_row_oper, i_col_oper] = enc0025_list_oper[idx]
        enc005_arr_psf[i_row_psf, i_col_psf] = enc005_list_psf[idx]
        enc005_arr_oper[i_row_oper, i_col_oper] = enc005_list_oper[idx]
        enc01_arr_psf[i_row_psf, i_col_psf] = enc01_list_psf[idx]
        enc01_arr_oper[i_row_oper, i_col_oper] = enc01_list_oper[idx]

    sr_arr_psf *= 100
    sr_arr_oper *= 100
    sr_arr_marechal *= 100
    fwhm_a_arr_psf *= 1000
    fwhm_g_arr_psf *= 1000
    fwhm_arr_oper *= 1000
    enc0025_arr_psf *= 100
    enc0025_arr_oper *= 100
    enc005_arr_psf *= 100
    enc005_arr_oper *= 100
    enc01_arr_psf *= 100
    enc01_arr_oper *= 100

    # Compare metrics
    sr_diff = sr_arr_oper - sr_arr_psf
    sr_ratio = sr_arr_oper / sr_arr_psf

    if fwhm_sim == 'arithmetic':
        fwhm_arr_psf = fwhm_a_arr_psf
    else:
        fwhm_arr_psf = fwhm_g_arr_psf

    fwhm_diff = fwhm_arr_oper - fwhm_arr_psf
    fwhm_ratio = fwhm_arr_oper / fwhm_arr_psf
    fwhm_ell_diff = fwhm_ell_arr_oper - fwhm_ell_arr_psf
    fwhm_ell_ratio = fwhm_ell_arr_oper / fwhm_ell_arr_psf
    fwhm_pa_diff = fwhm_pa_arr_oper - fwhm_pa_arr_psf
    enc0025_diff = enc0025_arr_oper - enc0025_arr_psf
    enc0025_ratio = enc0025_arr_oper / enc0025_arr_psf
    enc005_diff = enc005_arr_oper - enc005_arr_psf
    enc005_ratio = enc005_arr_oper / enc005_arr_psf
    enc01_diff = enc01_arr_oper - enc01_arr_psf
    enc01_ratio = enc01_arr_oper / enc01_arr_psf

    # Select the field of regard
    regard_r_px = params_girmos.regard_r / \
        (zenith_arr[int((n_side - 1) / 2), 0] -
         zenith_arr[int((n_side - 1) / 2), 1])

    # Generate metrics plots
    plots_path = path.join(folder_in, 'plots', 'operation')

    if path.exists(plots_path):
        shutil.rmtree(plots_path)

    makedirs(plots_path)
    print("   - Plotting PSF metrics ...")
    psf.grid_plot(sr_diff, max_range, tick_step, regard_r_px,
                  "Strehl ratio difference ({0} band)".format(wl_name),
                  'sr_diff', plots_path, units="%")
    psf.grid_plot(sr_ratio, max_range, tick_step, regard_r_px,
                  "Strehl ratio ratio ({0} band)".format(wl_name), 'sr_ratio',
                  plots_path)
    psf.grid_plot(fwhm_diff, max_range, tick_step, regard_r_px,
                  "FWHM difference ({0} band)".format(wl_name), 'fwhm_diff',
                  plots_path, units="mas", color_r=True)
    psf.grid_plot(fwhm_ratio, max_range, tick_step, regard_r_px,
                  "FWHM ratio ({0} band)".format(wl_name), 'fwhm_ratio',
                  plots_path, color_r=True)
    psf.grid_plot(fwhm_ell_diff, max_range, tick_step, regard_r_px,
                  "FWHM ellipticity difference ({0} band)".format(wl_name),
                  'fwhm_ell_ diff', plots_path, color_r=True)
    psf.grid_plot(fwhm_ell_ratio, max_range, tick_step, regard_r_px,
                  "FWHM ellipticity ratio ({0} band)".format(wl_name),
                  'fwhm_ell_ratio', plots_path, color_r=True)
    psf.grid_plot(enc0025_diff, max_range, tick_step, regard_r_px,
                  "Encircled energy difference\nat 25 mas\" ({0} band)".format(
                      wl_name), 'enc0025_diff', plots_path, units="%")
    psf.grid_plot(enc0025_ratio, max_range, tick_step, regard_r_px,
                  "Encircled energy ratio\nat 25 mas\" ({0} band)".format(
                      wl_name), 'enc0025_ratio', plots_path)
    psf.grid_plot(enc005_diff, max_range, tick_step, regard_r_px,
                  "Encircled energy difference\nat 50 mas\" ({0} band)".format(
                      wl_name), 'enc005_diff', plots_path, units="%")
    psf.grid_plot(enc005_ratio, max_range, tick_step, regard_r_px,
                  "Encircled energy ratio\nat 50 mas\" ({0} band)".format(
                      wl_name), 'enc005_ratio', plots_path)
    psf.grid_plot(enc01_diff, max_range, tick_step, regard_r_px,
                  "Encircled energy difference\nat 100 mas\" ({0} band)".format(
                      wl_name), 'enc001_diff', plots_path, units="%")
    psf.grid_plot(enc01_ratio, max_range, tick_step, regard_r_px,
                  "Encircled energy ratio\nat 100 mas\" ({0} band)".format(
                      wl_name), 'enc001_ratio', plots_path)

    # Generate statistics plots
    psf.stats_plot(sr_diff, zenith_arr, params_girmos.regard_r, "SR", '.1f',
                   "Strehl ratio difference ({0} band)".format(wl_name),
                   'stats_sr_diff', plots_path, units="%")
    psf.stats_plot(sr_ratio, zenith_arr, params_girmos.regard_r, "Ratio", '.2f',
                   "Strehl ratio ratio ({0} band)".format(wl_name),
                   'stats_sr_ratio', plots_path, ratio=True)
    psf.triple_stats_plot(sr_arr_psf, sr_arr_oper, sr_arr_marechal, zenith_arr,
                          params_girmos.regard_r, "SR", "Simulation",
                          "Analytic", "Maréchal",
                          "Strehl ratio ({0} band)".format(wl_name),
                          'stats_sr_both', plots_path, units="%")
    psf.stats_plot(fwhm_diff, zenith_arr, params_girmos.regard_r, "FWHM", '.1f',
                   "FWHM difference ({0} band)".format(wl_name),
                   'stats_fwhm_diff', plots_path, units="mas")
    psf.stats_plot(fwhm_ratio, zenith_arr, params_girmos.regard_r, "Ratio",
                   '.2f', "FWHM ratio ({0} band)".format(wl_name),
                   'stats_fwhm_ratio', plots_path, ratio=True)
    psf.double_stats_plot(fwhm_arr_psf, fwhm_arr_oper, zenith_arr,
                          params_girmos.regard_r, "FWHM", "Simulation",
                          "Analytic", "FWHM ({0} band)".format(wl_name),
                          'stats_fwhm_both', plots_path,
                          diffr_lim=(1.028 * wl * fwhm_co *
                                     params_girmos.rad_to_arcsec * 1e3 / psf.d),
                          units="mas")
    psf.stats_plot(enc0025_diff, zenith_arr, params_girmos.regard_r, "EE",
                   '.2f',
                   "Encircled energy difference\nat 25 mas ({0} band)".format(
                       wl_name), 'stats_enc0025_diff', plots_path, units="%")
    psf.stats_plot(enc0025_ratio, zenith_arr, params_girmos.regard_r, "Ratio",
                   '.2f',
                   "Encircled energy ratio\nat 25 mas ({0} band)".format(
                       wl_name), 'stats_enc0025_ratio', plots_path, ratio=True)
    psf.double_stats_plot(enc0025_arr_psf, enc0025_arr_oper, zenith_arr,
                          params_girmos.regard_r, "EE", "Simulation",
                          "Analytic",
                          "Encircled energy\nat 25 mas ({0} band)".format(
                              wl_name), 'stats_enc0025_both', plots_path,
                          units="%")
    psf.stats_plot(enc005_diff, zenith_arr, params_girmos.regard_r, "EE", '.2f',
                   "Encircled energy difference\nat 50 mas ({0} band)".format(
                       wl_name), 'stats_enc005_diff', plots_path, units="%")
    psf.stats_plot(enc005_ratio, zenith_arr, params_girmos.regard_r, "Ratio",
                   '.2f',
                   "Encircled energy ratio at\n50 mas ({0} band)".format(
                       wl_name), 'stats_enc005_ratio', plots_path, ratio=True)
    psf.double_stats_plot(enc005_arr_psf, enc005_arr_oper, zenith_arr,
                          params_girmos.regard_r, "EE", "Simulation",
                          "Analytic",
                          "Encircled energy\nat 50 mas ({0} band)".format(
                              wl_name), 'stats_enc005_both', plots_path,
                          units="%")
    psf.stats_plot(enc01_diff, zenith_arr, params_girmos.regard_r, "EE", '.2f',
                   "Encircled energy difference\nat 100 mas ({0} band)".format(
                       wl_name), 'stats_enc01_diff', plots_path, units="%")
    psf.stats_plot(enc01_ratio, zenith_arr, params_girmos.regard_r, "Ratio",
                   '.2f',
                   "Encircled energy ratio at\n100 mas ({0} band)".format(
                       wl_name), 'stats_enc01_ratio', plots_path, ratio=True)
    psf.double_stats_plot(enc01_arr_psf, enc01_arr_oper, zenith_arr,
                          params_girmos.regard_r, "EE", "Simulation",
                          "Analytic",
                          "Encircled energy\nat 100 mas ({0} band)".format(
                              wl_name), 'stats_enc01_both', plots_path,
                          units="%")

    # Generate meta-metrics
    print("   - Recording PSF meta-metrics ...")
    idx_meta = np.where(zenith_arr <= params_girmos.regard_r)
    sr_min_diff = np.min(sr_diff[idx_meta])
    sr_min_ratio = np.min(sr_ratio[idx_meta])
    fwhm_min_diff = np.min(fwhm_diff[idx_meta])
    fwhm_min_ratio = np.min(fwhm_ratio[idx_meta])
    enc0025_min_diff = np.min(enc0025_diff[idx_meta])
    enc0025_min_ratio = np.min(enc0025_ratio[idx_meta])
    enc005_min_diff = np.min(enc005_diff[idx_meta])
    enc005_min_ratio = np.min(enc005_ratio[idx_meta])
    enc01_min_diff = np.min(enc01_diff[idx_meta])
    enc01_min_ratio = np.min(enc01_ratio[idx_meta])
    sr_max_diff = np.max(sr_diff[idx_meta])
    sr_max_ratio = np.max(sr_ratio[idx_meta])
    fwhm_max_diff = np.max(fwhm_diff[idx_meta])
    fwhm_max_ratio = np.max(fwhm_ratio[idx_meta])
    enc0025_max_diff = np.max(enc0025_diff[idx_meta])
    enc0025_max_ratio = np.max(enc0025_ratio[idx_meta])
    enc005_max_diff = np.max(enc005_diff[idx_meta])
    enc005_max_ratio = np.max(enc005_ratio[idx_meta])
    enc01_max_diff = np.max(enc01_diff[idx_meta])
    enc01_max_ratio = np.max(enc01_ratio[idx_meta])
    sr_mean_diff = np.mean(sr_diff[idx_meta])
    sr_mean_ratio = np.mean(sr_ratio[idx_meta])
    fwhm_mean_diff = np.mean(fwhm_diff[idx_meta])
    fwhm_mean_ratio = np.mean(fwhm_ratio[idx_meta])
    enc0025_mean_diff = np.mean(enc0025_diff[idx_meta])
    enc0025_mean_ratio = np.mean(enc0025_ratio[idx_meta])
    enc005_mean_diff = np.mean(enc005_diff[idx_meta])
    enc005_mean_ratio = np.mean(enc005_ratio[idx_meta])
    enc01_mean_diff = np.mean(enc01_diff[idx_meta])
    enc01_mean_ratio = np.mean(enc01_ratio[idx_meta])
    sr_std_diff = np.std(sr_diff[idx_meta])
    sr_std_ratio = np.std(sr_ratio[idx_meta])
    fwhm_std_diff = np.std(fwhm_diff[idx_meta])
    fwhm_std_ratio = np.std(fwhm_ratio[idx_meta])
    enc0025_std_diff = np.std(enc0025_diff[idx_meta])
    enc0025_std_ratio = np.std(enc0025_ratio[idx_meta])
    enc005_std_diff = np.std(enc005_diff[idx_meta])
    enc005_std_ratio = np.std(enc005_ratio[idx_meta])
    enc01_std_diff = np.std(enc01_diff[idx_meta])
    enc01_std_ratio = np.std(enc01_ratio[idx_meta])
    sr_cv_diff = sr_std_diff / sr_mean_diff
    sr_cv_ratio = sr_std_ratio / sr_mean_ratio
    fwhm_cv_diff = fwhm_std_diff / fwhm_mean_diff
    fwhm_cv_ratio = fwhm_std_ratio / fwhm_mean_ratio
    enc0025_cv_diff = enc0025_std_diff / enc0025_mean_diff
    enc0025_cv_ratio = enc0025_std_ratio / enc0025_mean_ratio
    enc005_cv_diff = enc005_std_diff / enc005_mean_diff
    enc005_cv_ratio = enc005_std_ratio / enc005_mean_ratio
    enc01_cv_diff = enc01_std_diff / enc01_mean_diff
    enc01_cv_ratio = enc01_std_ratio / enc01_mean_ratio
    header_str = "\n{0} band\n\n".format(wl_name)
    sr_str = ("SR\n   Difference\n      Min: {0:.1f} %\n      " +
              "Max: {1:.1f} %\n      Mean: {2:.1f} %\n      " +
              "STD: {3:.1f} %\n      CV: {4:.3f}\n   Ratio\n      " +
              "Min: {5:.1f}\n      Max: {6:.1f}\n      Mean: {7:.1f}\n      " +
              "STD: {8:.1f}\n      CV: {9:.3f}\n\n").format(
        sr_min_diff, sr_max_diff, sr_mean_diff, sr_std_diff, sr_cv_diff,
        sr_min_ratio, sr_max_ratio, sr_mean_ratio, sr_std_ratio, sr_cv_ratio)
    fwhm_str = ("FWHM\n   Difference\n      Min: {0:.1f} mas\n      " +
                "Max: {1:.1f} mas\n      Mean: {2:.1f} mas\n      " +
                "STD: {3:.1f} mas\n      CV: {4:.3f}\n   Ratio\n      " +
                "Min: {5:.1f}\n      Max: {6:.1f}\n      " +
                "Mean: {7:.1f}\n      STD: {8:.1f}\n      " +
                "CV: {9:.3f}\n\n").format(fwhm_min_diff, fwhm_max_diff,
                                          fwhm_mean_diff, fwhm_std_diff,
                                          fwhm_cv_diff, fwhm_min_ratio,
                                          fwhm_max_ratio, fwhm_mean_ratio,
                                          fwhm_std_ratio, fwhm_cv_ratio)
    enc0025_str = ("Encircled energy at 25 mas\n   Difference\n      " +
                   "Min: {0:.1f} %\n      Max: {1:.1f} %\n      " +
                   "Mean: {2:.1f} %\n      STD: {3:.1f} %\n      " +
                   "CV: {4:.3f}\n   Ratio\n      Min: {5:.1f}\n      " +
                   "Max: {6:.1f}\n      Mean: {7:.1f}\n      " +
                   "STD: {8:.1f}\n      CV: {9:.3f}\n\n").format(
        enc0025_min_diff, enc0025_max_diff, enc0025_mean_diff, enc0025_std_diff,
        enc0025_cv_diff, enc0025_min_ratio, enc0025_max_ratio,
        enc0025_mean_ratio, enc0025_std_ratio, enc0025_cv_ratio)
    enc005_str = ("Encircled energy at 50 mas\n   Difference\n      " +
                  "Min: {0:.1f} %\n      Max: {1:.1f} %\n      " +
                  "Mean: {2:.1f} %\n      STD: {3:.1f} %\n      " +
                  "CV: {4:.3f}\n   Ratio\n      Min: {5:.1f}\n      " +
                  "Max: {6:.1f}\n      Mean: {7:.1f}\n      " +
                  "STD: {8:.1f}\n      CV: {9:.3f}\n\n").format(
        enc005_min_diff, enc005_max_diff, enc005_mean_diff, enc005_std_diff,
        enc005_cv_diff, enc005_min_ratio, enc005_max_ratio, enc005_mean_ratio,
        enc005_std_ratio, enc005_cv_ratio)
    enc01_str = ("Encircled energy at 100 mas\n   Difference\n      " +
                 "Min: {0:.1f} %\n      Max: {1:.1f} %\n      " +
                 "Mean: {2:.1f} %\n      STD: {3:.1f} %\n      " +
                 "CV: {4:.3f}\n   Ratio\n      Min: {5:.1f}\n      " +
                 "Max: {6:.1f}\n      Mean: {7:.1f}\n      " +
                 "STD: {8:.1f}\n      CV: {9:.3f}\n").format(
        enc01_min_diff, enc01_max_diff, enc01_mean_diff, enc01_std_diff,
        enc01_cv_diff, enc01_min_ratio, enc01_max_ratio, enc01_mean_ratio,
        enc01_std_ratio, enc01_cv_ratio)
    plots_meta_path = path.join(plots_path, 'meta')
    makedirs(plots_meta_path)
    meta_file = open(path.join(plots_meta_path, 'metametrics.txt'), 'w')
    meta_file.write(header_str)
    meta_file.write(sr_str)
    meta_file.write(fwhm_str)
    meta_file.write(enc0025_str)
    meta_file.write(enc005_str)
    meta_file.write(enc01_str)
    meta_file.close()

print("Program terminated\n")
