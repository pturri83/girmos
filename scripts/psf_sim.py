"""Program to generate and analyze a grid of GIRMOS PSF from a grid of simulated
WF residuals.
The results are saved in the [wl_name] subfolder.
"""


import copy
import glob
from os import makedirs, path, remove
import pickle
import re
import time

from matplotlib import pyplot as plt
import numpy as np

from source import misc_girmos, params_girmos, psf


# Data parameters
folder = '/Volumes/Astro/Data/GIRMOS/Simulations'  # Simulations folder
sim_n = 3  # Simulation number
start_from = []  # Resume analysis from this [band, NCPA RMS (nm)] set
# (use [] to start from the beginning)
test = 62  # Index of a single PSF to test (use None for full simulation)

# Analysis parameters
ncpas = [0, 50, 100]  # NCPA RMS tested (nm)

# Find files
print("\nProgram started")
start_time = time.time()
folder = path.join(folder, ('sim_' + str(sim_n)))
files = glob.glob(path.join(folder, 'wfr', 'n_*_x_*_y_*.mat'))
n_files = len(files)

# Use different wavelengths and NCPAs
if start_from:
    wl_start = np.arange(np.where(np.array(params_girmos.wl_names) ==
                                  start_from[0])[0],
                         len(params_girmos.wl_names))
    wl_names_ok = np.array(params_girmos.wl_names)[wl_start]
    wls_ok = np.array(params_girmos.wls)[wl_start]
else:
    wl_names_ok = np.array(params_girmos.wl_names)
    wls_ok = np.array(params_girmos.wls)

bookmark = path.join(folder, 'bookmark.txt')

for wl_name in wl_names_ok:

    # Choose wavelength
    print("- {0} band".format(wl_name))
    wl = wls_ok[np.where(np.array(wl_names_ok) == wl_name)[0][0]] * 1e-6
    folder_out = path.join(folder, 'psf', wl_name)

    if not path.exists(folder_out):
        makedirs(folder_out)

    # Choose NCPAs
    if start_from and (wl_name == start_from[0]):
        ncpa_start = np.arange(np.where(np.array(ncpas) == start_from[1])[0],
                               len(ncpas))
        ncpas_ok = np.array(ncpas)[ncpa_start]
    else:
        ncpas_ok = np.array(ncpas)

    for ncpa in ncpas_ok:
        print("   - {0:d} nm RMS NCPA ...".format(ncpa))
        folder_ncpa = path.join(folder, 'ncpa')
        file_tmp = files[0]
        wfr_tmp = psf.wfr_load(file_tmp)
        wfr_shape = wfr_tmp.shape

        if (wl_name == params_girmos.wl_names[0]) and (ncpa == 0):

            # Create NCPA
            n_zern = 500  # Number of Zernike modes used for NCPA
            print("      - Calculating NCPA ...")

            if not path.exists(folder_ncpa):
                makedirs(folder_ncpa)

            ncpa_idx = misc_girmos.rad_az(n_zern)
            ncpa_coeff = np.random.normal(loc=0, scale=1, size=n_zern)
            ncpa_coeff_ones = np.ones(n_zern)
            ncpa_coeff[: 3] = 0
            ncpa_coeff_ones[: 3] = 0
            ncpa_coeff[3:] /= ncpa_idx[3:, 0]
            ncpa_coeff_ones[3:] /= ncpa_idx[3:, 0]
            ncpa_coeff[3:] /= np.exp((params_girmos.act - 1) -
                                     ncpa_idx[3:, 0]) + 1
            ncpa_coeff_ones_orig = copy.copy(ncpa_coeff_ones)
            ncpa_coeff_ones[3:] /= np.exp((params_girmos.act - 1) -
                                          ncpa_idx[3:, 0]) + 1
            wfr_ncpa = misc_girmos.zern_sum(wfr_shape[0], n_zern, ncpa_coeff)
            y_mesh, x_mesh = np.indices(wfr_shape[0: 2])
            rad_mask = (wfr_shape[0] - 1) / 2
            dist_mask = np.hypot((y_mesh - rad_mask), (x_mesh - rad_mask))
            wfr_mask = np.where(dist_mask <= rad_mask)
            wfr_ncpa_std = np.std(wfr_ncpa[wfr_mask])
            wfr_ncpa /= wfr_ncpa_std

            # Save NCPA
            file_out_ncpa_coeff_orig = open(path.join(folder_ncpa,
                                                      'ncpa_coeff_orig.pkl'),
                                            'wb+')
            pickle.dump(ncpa_coeff_ones_orig, file_out_ncpa_coeff_orig)
            file_out_ncpa_coeff_orig.close()
            file_out_ncpa_coeff = open(path.join(folder_ncpa, 'ncpa_coeff.pkl'),
                                       'wb+')
            pickle.dump(ncpa_coeff_ones, file_out_ncpa_coeff)
            file_out_ncpa_coeff.close()
            file_out_ncpa = open(path.join(folder_ncpa, 'ncpa.pkl'), 'wb+')
            pickle.dump(wfr_ncpa, file_out_ncpa)
            file_out_ncpa.close()

            # Plot NCPA
            psf.wfr_plot(np.expand_dims(wfr_ncpa, axis=2),
                         title="NCPA Residual Wavefront", bar=False,
                         save=path.join(folder_ncpa, 'ncpa_wfr.png'))
            plt.close()
            fig = plt.figure(figsize=(7, 5))
            ax = fig.add_subplot(111)
            ax.set_position([0.15, 0.15, 0.7, 0.7])
            ax.plot(ncpa_coeff_ones_orig, 'ob', markersize=3, label="Original")
            ax.plot(ncpa_coeff_ones, 'or', markersize=3, label="Corrected")
            plt.yscale('log')
            ax.legend()
            ax.set_xlabel("Mode #")
            ax.set_ylabel("RMS")
            plt.title("NCPA spectrum")
            plt.savefig(path.join(folder_ncpa, 'ncpa_modes.png'))
            plt.close()
        else:

            # Load NCPA
            file_in_ncpa = open(path.join(folder_ncpa, 'ncpa.pkl'), 'rb')
            wfr_ncpa = pickle.load(file_in_ncpa)
            file_in_ncpa.close()

        wfr_ncpa *= ncpa * 1e-9
        wfr_ncpa_stack = np.repeat(wfr_ncpa[:, :, np.newaxis], wfr_shape[2],
                                   axis=2)

        # Plot NCPA
        if ncpa == 0:
            psf.wfr_plot(np.expand_dims(wfr_ncpa, axis=2),
                         title="NCPA Residual Wavefront ({0} nm RMS)".format(
                             ncpa), bar=False,
                         save=path.join(folder_ncpa,
                                        'ncpa_wfr_{0}.png'.format(ncpa)))
        else:
            psf.wfr_plot(np.expand_dims(wfr_ncpa, axis=2),
                         title="NCPA Residual Wavefront ({0} nm RMS)".format(
                             ncpa),
                         save=path.join(folder_ncpa,
                                        'ncpa_wfr_{0}.png'.format(ncpa)))
        plt.close()

        # Prepare output
        filename = []
        x = np.zeros(n_files)
        y = np.zeros(n_files)
        zenith = np.zeros(n_files)
        sr = np.zeros(n_files)
        fwhm_maj = np.zeros(n_files)
        fwhm_min = np.zeros(n_files)
        fwhm_a = np.zeros(n_files)
        fwhm_g = np.zeros(n_files)
        fwhm_ell = np.zeros(n_files)
        fwhm_pa = np.zeros(n_files)
        ens_rad = []
        ens = []
        enc_rad = []
        enc = []
        ens25 = np.zeros(n_files)
        ens50 = np.zeros(n_files)
        ens80 = np.zeros(n_files)
        enc25 = np.zeros(n_files)
        enc50 = np.zeros(n_files)
        enc80 = np.zeros(n_files)
        ens0025 = np.zeros(n_files)
        ens005 = np.zeros(n_files)
        ens01 = np.zeros(n_files)
        enc0025 = np.zeros(n_files)
        enc005 = np.zeros(n_files)
        enc01 = np.zeros(n_files)
        nea = np.zeros(n_files)
        psfs_xy = np.empty(1)
        psfs_arr = np.empty(1)
        psf_edges = None
        psf_where = None
        psf_edges_over = None
        psf_where_over = None
        psfs_over_xy = np.empty(1)
        psfs_over_arr = np.empty(1)
        ps_over = 0

        # Process WFRs
        if test:
            i_files = [0, test]
        else:
            i_files = range(n_files)

        for i_file in i_files:
            print("\r      - Calculating PSF # {0}/{1} ...".format(
                (i_file + 1), n_files), end="")

            if i_file == test:
                test_flag = True
            else:
                test_flag = False

            bookmark_file = open(bookmark, 'w')
            bookmark_file.write(wl_name + " band, " + str(ncpa) + " nm, PSF # "
                                + str(i_file + 1) + "   (i_file = " +
                                str(i_file) + ")")
            bookmark_file.close()
            file = files[i_file]
            file_only = path.split(file)[1]
            filename.append(file_only)
            file_split = re.split('n_|_x_|_y_|.mat', file_only)
            x[i_file] = float(file_split[2])
            y[i_file] = float(file_split[3])
            zenith[i_file] = np.hypot(x[i_file], y[i_file])
            wfr = psf.wfr_load(file)
            wfr += wfr_ncpa_stack
            metr, psfs_i, psfs_xy, psf_edges, psf_where, psfs_over_i, \
                psfs_over_xy, psf_edges_over, psf_where_over, ps_over = \
                psf.psf_metrics(wfr, wl, params_girmos.ps, psf_edges=psf_edges,
                                psf_where=psf_where,
                                psf_edges_over=psf_edges_over,
                                psf_where_over=psf_where_over,
                                test_flag=test_flag)
            sr[i_file] = metr['sr']
            fwhm_maj[i_file] = metr['fwhm_maj']
            fwhm_min[i_file] = metr['fwhm_min']
            fwhm_a[i_file] = metr['fwhm_a']
            fwhm_g[i_file] = metr['fwhm_g']
            fwhm_ell[i_file] = metr['fwhm_ell']
            fwhm_pa[i_file] = metr['fwhm_pa']

            if i_file == 0:
                ens_rad = metr['ens_rad']
                ens = metr['ens']
                enc_rad = metr['enc_rad']
                enc = metr['enc']
            else:
                ens = np.vstack((ens, metr['ens']))
                enc = np.vstack((enc, metr['enc']))

            ens25[i_file] = metr['ens25']
            ens50[i_file] = metr['ens50']
            ens80[i_file] = metr['ens80']
            enc25[i_file] = metr['enc25']
            enc50[i_file] = metr['enc50']
            enc80[i_file] = metr['enc80']
            ens0025[i_file] = metr['ens0025']
            ens005[i_file] = metr['ens005']
            ens01[i_file] = metr['ens01']
            enc0025[i_file] = metr['enc0025']
            enc005[i_file] = metr['enc005']
            enc01[i_file] = metr['enc01']
            nea[i_file] = metr['nea']
            del metr

            if psfs_arr.size == 1:
                psf_side = psfs_i.shape[0]
                psfs_arr = np.zeros([psf_side, psf_side, n_files])
                psf_over_side = psfs_over_i.shape[0]
                psfs_over_arr = np.zeros([psf_over_side, psf_over_side,
                                          n_files])

            psfs_arr[:, :, i_file] = psfs_i
            psfs_over_arr[:, :, i_file] = psfs_over_i

        # Compare PSFs
        print("\n      - Analyzing PSF differencies ...")
        idx_center = np.where((np.abs(x) == np.abs(x).min()) &
                              (np.abs(y) == np.abs(y).min()))[0][0]
        psf_res_std, psf_res_sum, psfs_over_res_arr = \
            psf.psf_diff(psfs_over_arr, psf_where_over, idx_center)

        # Prepare the grid
        n_side = int(np.sqrt(n_files))
        x_sort = sorted(x)
        max_range = int(np.mean(x_sort[-n_side:]))
        steps = np.linspace(-max_range, max_range, num=n_side)
        col = np.zeros(n_files)
        row = np.zeros(n_files)

        for i_row in range(n_side):
            for i_col in range(n_side):
                x_cell = steps[i_col]
                y_cell = steps[i_row]
                dr = np.hypot((x - x_cell), (y - y_cell))
                idx = np.argmin(dr)
                col[idx] = i_col
                row[idx] = i_row

        # Save output
        print("      - Saving results ...")
        metrs = {'wl_name': wl_name, 'wl': wl, 'ps': params_girmos.ps,
                 'n': n_files, 'file': filename, 'x': x, 'y': y,
                 'zenith': zenith, 'col': col, 'row': row,
                 'max_range': max_range, 'sr': sr, 'fwhm_maj': fwhm_maj,
                 'fwhm_min': fwhm_min, 'fwhm_a': fwhm_a, 'fwhm_g': fwhm_g,
                 'fwhm_ell': fwhm_ell, 'fwhm_pa': fwhm_pa, 'ens_rad': ens_rad,
                 'ens': ens, 'enc_rad': enc_rad, 'enc': enc, 'ens25': ens25,
                 'ens50': ens50, 'ens80': ens80, 'enc25': enc25, 'enc50': enc50,
                 'enc80': enc80, 'ens0025': ens0025, 'ens005': ens005,
                 'ens01': ens01, 'enc0025': enc0025, 'enc005': enc005,
                 'enc01': enc01, 'nea': nea, 'psf_res_std': psf_res_std,
                 'psf_res_sum': psf_res_sum}
        psfs = {'ps': params_girmos.ps, 'n': n_files, 'x': x, 'y': y,
                'col': col, 'row': row, 'max_range': max_range,
                'xy_arr': psfs_xy, 'psfs_arr': psfs_arr, 'psf_where': psf_where}
        psfs_over = {'ps': ps_over, 'n': n_files, 'x': x, 'y': y, 'col': col,
                     'row': row, 'max_range': max_range, 'xy_arr': psfs_over_xy,
                     'psfs_arr': psfs_over_arr, 'psf_where': psf_where_over}
        psfs_over_res = {'ps': ps_over, 'n': n_files, 'x': x, 'y': y,
                         'col': col, 'row': row, 'max_range': max_range,
                         'xy_arr': psfs_over_xy, 'psfs_arr': psfs_over_res_arr,
                         'psf_where': psf_where_over}
        file_out_metrs = open(path.join(folder_out, ('metrics_' + str(ncpa) +
                                                     '.pkl')), 'wb+')
        pickle.dump(metrs, file_out_metrs)
        file_out_metrs.close()
        file_out_psfs = open(path.join(folder_out, ('psfs_' + str(ncpa) +
                                                    '.pkl')), 'wb+')
        pickle.dump(psfs, file_out_psfs, protocol=4)
        file_out_psfs.close()
        file_out_psfs_over = open(path.join(folder_out,
                                            ('psfs_over_' + str(ncpa) +
                                             '.pkl')), 'wb+')
        pickle.dump(psfs_over, file_out_psfs_over, protocol=4)
        file_out_psfs_over.close()
        file_out_psfs_over_res = open(path.join(folder_out,
                                                ('psfs_over_res_' + str(ncpa) +
                                                 '.pkl')), 'wb+')
        pickle.dump(psfs_over_res, file_out_psfs_over_res, protocol=4)
        file_out_psfs_over_res.close()

# End program
remove(bookmark)
end_time = time.time()
elapsed_time = end_time - start_time
elapsed_d = int(np.floor(elapsed_time / (60 * 60 * 24)))
elapsed_h = int(np.floor((elapsed_time / (60 * 60)) - (elapsed_d * 24)))
elapsed_m = int(np.floor((elapsed_time / 60) - (elapsed_h * 60) -
                (elapsed_d * 60 * 24)))
elapsed_s = int(np.floor(elapsed_time - (elapsed_m * 60) -
                (elapsed_h * 60 * 60) - (elapsed_d * 60 * 60 * 24)))
print("Time elapsed: {0:02d}d:{1:02d}h:{2:02d}m:{3:02d}s".format(elapsed_d,
                                                                 elapsed_h,
                                                                 elapsed_m,
                                                                 elapsed_s))
print("Program terminated\n")
