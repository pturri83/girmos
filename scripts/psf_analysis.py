"""Program to plot the results of the analysis of a grid of GIRMOS PSF generated
from a grid of simulated WF residuals.
"""


import glob
from os import makedirs, path
import pickle
import re
import shutil
import time

import numpy as np

from source import params_girmos, psf


# Data parameters
folder_data = '/Volumes/Astro/Data/GIRMOS/Simulations'  # Simulations folder
sim_n = 1  # Simulation number
wvls = 'k'  # Wavelengths to analyze ('jh' for J and H, 'k' for Ks)

# Graphic parameters
tick_step = 20  # Axes' tick step (")
ell_scale = 18  # Ellipticity plot scale
psf_res_zero = 0.0001  # PSF values colored as zero in a symmetric logarithmic
# scale
psf_zoom = 0.3  # PSF zoom half-side (")
psf_tick = 0.1  # PSF ticks (")
psf_grid_side = 7  # Number of PSFs to show on the side of the grid

# Use different wavelengths and NCPAs
print("\nProgram started")
folder_data = path.join(folder_data, ('sim_' + str(sim_n)), 'psf')
wl_names = np.array(params_girmos.wl_names)

if wvls == 'jh':
    wl_names = np.array(wl_names[0: -1])
else:
    wl_names = np.array([wl_names[-1]])

start_time = time.time()

for wl_name in wl_names:

    # Find NCPAs
    print("- {0} band".format(wl_name))
    folders = glob.glob(path.join(folder_data, wl_name, 'metrics_*.pkl'))
    ncpas = [int(re.split('[_.]', i)[-2]) for i in folders]
    ncpas.sort()

    # Calculate plot limits
    print("   - Measuring values' limits ...")
    sr_lim = [1e10, 0]
    fwhm_maj_lim = [1e10, 0]
    fwhm_min_lim = [1e10, 0]
    fwhm_a_lim = [1e10, 0]
    fwhm_g_lim = [1e10, 0]
    fwhm_ell_lim = [1e10, 0]
    ens25_lim = [1e10, 0]
    ens50_lim = [1e10, 0]
    ens80_lim = [1e10, 0]
    enc25_lim = [1e10, 0]
    enc50_lim = [1e10, 0]
    enc80_lim = [1e10, 0]
    ens0025_lim = [1e10, 0]
    ens005_lim = [1e10, 0]
    ens01_lim = [1e10, 0]
    enc0025_lim = [1e10, 0]
    enc005_lim = [1e10, 0]
    enc01_lim = [1e10, 0]
    nea_lim = [1e10, 0]
    psf_res_sum_lim = [1e10, 0]
    psf_res_std_lim = [1e10, 0]

    for ncpa in ncpas:
        folder_in = path.join(folder_data, wl_name)
        file_in = open(path.join(folder_in, ('metrics_' + str(ncpa) + '.pkl')),
                       'rb')
        metr = pickle.load(file_in)
        file_in.close()
        sr_lim = [np.min([sr_lim[0], np.min(metr['sr'])]),
                  np.max([sr_lim[1], np.max(metr['sr'])])]
        fwhm_maj_lim = [np.min([fwhm_maj_lim[0], np.min(metr['fwhm_maj'])]),
                        np.max([fwhm_maj_lim[1], np.max(metr['fwhm_maj'])])]
        fwhm_min_lim = [np.min([fwhm_min_lim[0], np.min(metr['fwhm_min'])]),
                        np.max([fwhm_min_lim[1], np.max(metr['fwhm_min'])])]
        fwhm_a_lim = [np.min([fwhm_a_lim[0], np.min(metr['fwhm_a'])]),
                      np.max([fwhm_a_lim[1], np.max(metr['fwhm_a'])])]
        fwhm_g_lim = [np.min([fwhm_g_lim[0], np.min(metr['fwhm_g'])]),
                      np.max([fwhm_g_lim[1], np.max(metr['fwhm_g'])])]
        fwhm_ell_lim = [np.min([fwhm_ell_lim[0], np.min(metr['fwhm_ell'])]),
                        np.max([fwhm_ell_lim[1], np.max(metr['fwhm_ell'])])]
        ens25_lim = [np.min([ens25_lim[0], np.min(metr['ens25'])]),
                     np.max([ens25_lim[1], np.max(metr['ens25'])])]
        ens50_lim = [np.min([ens50_lim[0], np.min(metr['ens50'])]),
                     np.max([ens50_lim[1], np.max(metr['ens50'])])]
        ens80_lim = [np.min([ens80_lim[0], np.min(metr['ens80'])]),
                     np.max([ens80_lim[1], np.max(metr['ens80'])])]
        enc25_lim = [np.min([enc25_lim[0], np.min(metr['enc25'])]),
                     np.max([enc25_lim[1], np.max(metr['enc25'])])]
        enc50_lim = [np.min([enc50_lim[0], np.min(metr['enc50'])]),
                     np.max([enc50_lim[1], np.max(metr['enc50'])])]
        enc80_lim = [np.min([enc80_lim[0], np.min(metr['enc80'])]),
                     np.max([enc80_lim[1], np.max(metr['enc80'])])]
        ens0025_lim = [np.min([ens0025_lim[0], np.min(metr['ens0025'])]),
                       np.max([ens0025_lim[1], np.max(metr['ens0025'])])]
        ens005_lim = [np.min([ens005_lim[0], np.min(metr['ens005'])]),
                      np.max([ens005_lim[1], np.max(metr['ens005'])])]
        ens01_lim = [np.min([ens01_lim[0], np.min(metr['ens01'])]),
                     np.max([ens01_lim[1], np.max(metr['ens01'])])]
        enc0025_lim = [np.min([enc0025_lim[0], np.min(metr['enc0025'])]),
                       np.max([enc0025_lim[1], np.max(metr['enc0025'])])]
        enc005_lim = [np.min([enc005_lim[0], np.min(metr['enc005'])]),
                      np.max([enc005_lim[1], np.max(metr['enc005'])])]
        enc01_lim = [np.min([enc01_lim[0], np.min(metr['enc01'])]),
                     np.max([enc01_lim[1], np.max(metr['enc01'])])]
        nea_lim = [np.min([nea_lim[0], np.min(metr['nea'])]),
                   np.max([nea_lim[1], np.max(metr['nea'])])]
        psf_res_sum_lim = [np.min([psf_res_sum_lim[0],
                                   np.min(metr['psf_res_sum'])]),
                           np.max([psf_res_sum_lim[1],
                                   np.max(metr['psf_res_sum'])])]
        psf_res_std_lim = [np.min([psf_res_std_lim[0],
                                   np.min(metr['psf_res_std'])]),
                           np.max([psf_res_std_lim[1],
                                   np.max(metr['psf_res_std'])])]

    sr_lim = np.array(sr_lim) * 100
    fwhm_maj_lim = np.array(fwhm_maj_lim) * 1000
    fwhm_min_lim = np.array(fwhm_min_lim) * 1000
    fwhm_a_lim = np.array(fwhm_a_lim) * 1000
    fwhm_g_lim = np.array(fwhm_g_lim) * 1000
    fwhm_ell_lim = np.array(fwhm_ell_lim)
    ens25_lim = np.array(ens25_lim) * 1000
    ens50_lim = np.array(ens50_lim) * 1000
    ens80_lim = np.array(ens80_lim) * 1000
    enc25_lim = np.array(enc25_lim) * 1000
    enc50_lim = np.array(enc50_lim) * 1000
    enc80_lim = np.array(enc80_lim) * 1000
    ens0025_lim = np.array(ens0025_lim) * 100
    ens005_lim = np.array(ens005_lim) * 100
    ens01_lim = np.array(ens01_lim) * 100
    enc0025_lim = np.array(enc0025_lim) * 100
    enc005_lim = np.array(enc005_lim) * 100
    enc01_lim = np.array(enc01_lim) * 100
    nea_lim = np.array(nea_lim)
    psf_res_sum_lim = np.array(psf_res_sum_lim) * 100
    psf_res_std_lim = np.array(psf_res_std_lim) * 100

    # Plot metrics
    for ncpa in ncpas:

        # Load metrics
        print("   - {0:d} nm RMS NCPA ...".format(ncpa))
        folder_in = path.join(folder_data, wl_name)
        file_in = open(path.join(folder_in, ('metrics_' + str(ncpa) + '.pkl')),
                       'rb')
        metr = pickle.load(file_in)
        file_in.close()
        ps = metr['ps']
        n = metr['n']
        file_list = metr['file']
        x_list = metr['x']
        y_list = metr['y']
        zenith_list = metr['zenith']
        col_list = metr['col']
        row_list = metr['row']
        sr_list = metr['sr']
        fwhm_maj_list = metr['fwhm_maj']
        fwhm_min_list = metr['fwhm_min']
        fwhm_a_list = metr['fwhm_a']
        fwhm_g_list = metr['fwhm_g']
        fwhm_ell_list = metr['fwhm_ell']
        fwhm_pa_list = metr['fwhm_pa']
        ens_rad_list = metr['ens_rad']
        ens_list = metr['ens']
        enc_rad_list = metr['enc_rad']
        enc_list = metr['enc']
        ens25_list = metr['ens25']
        ens50_list = metr['ens50']
        ens80_list = metr['ens80']
        enc25_list = metr['enc25']
        enc50_list = metr['enc50']
        enc80_list = metr['enc80']
        ens0025_list = metr['ens0025']
        ens005_list = metr['ens005']
        ens01_list = metr['ens01']
        enc0025_list = metr['enc0025']
        enc005_list = metr['enc005']
        enc01_list = metr['enc01']
        nea_list = metr['nea']
        psf_res_sum_list = metr['psf_res_sum']
        psf_res_std_list = metr['psf_res_std']

        # Load PSFs
        print("      - Loading files ...")
        file_in = open(path.join(folder_in, ('psfs_' + str(ncpa) + '.pkl')),
                       'rb')
        psfs = pickle.load(file_in)
        file_in.close()
        file_in = open(path.join(folder_in,
                                 ('psfs_over_' + str(ncpa) + '.pkl')), 'rb')
        psfs_over = pickle.load(file_in)
        file_in.close()
        file_in = open(path.join(folder_in,
                                 ('psfs_over_res_' + str(ncpa) + '.pkl')), 'rb')
        psfs_over_res = pickle.load(file_in)
        file_in.close()

        # Position PSF parameters
        n_side = int(np.sqrt(n))
        max_range = metr['max_range']
        zenith_arr = np.zeros((n_side, n_side))
        file_arr = np.chararray((n_side, n_side))
        sr_arr = np.zeros((n_side, n_side))
        fwhm_maj_arr = np.zeros((n_side, n_side))
        fwhm_min_arr = np.zeros((n_side, n_side))
        fwhm_a_arr = np.zeros((n_side, n_side))
        fwhm_g_arr = np.zeros((n_side, n_side))
        fwhm_ell_arr = np.zeros((n_side, n_side))
        fwhm_pa_arr = np.zeros((n_side, n_side))
        ens25_arr = np.zeros((n_side, n_side))
        ens50_arr = np.zeros((n_side, n_side))
        ens80_arr = np.zeros((n_side, n_side))
        enc25_arr = np.zeros((n_side, n_side))
        enc50_arr = np.zeros((n_side, n_side))
        enc80_arr = np.zeros((n_side, n_side))
        ens0025_arr = np.zeros((n_side, n_side))
        ens005_arr = np.zeros((n_side, n_side))
        ens01_arr = np.zeros((n_side, n_side))
        enc0025_arr = np.zeros((n_side, n_side))
        enc005_arr = np.zeros((n_side, n_side))
        enc01_arr = np.zeros((n_side, n_side))
        nea_arr = np.zeros((n_side, n_side))
        psf_res_sum_arr = np.zeros((n_side, n_side))
        psf_res_std_arr = np.zeros((n_side, n_side))

        for idx in range(n):
            i_col = int(col_list[idx])
            i_row = int(row_list[idx])
            zenith_arr[i_row, i_col] = zenith_list[idx]
            file_arr[i_row, i_col] = file_list[idx]
            sr_arr[i_row, i_col] = sr_list[idx]
            fwhm_maj_arr[i_row, i_col] = fwhm_maj_list[idx]
            fwhm_min_arr[i_row, i_col] = fwhm_min_list[idx]
            fwhm_a_arr[i_row, i_col] = fwhm_a_list[idx]
            fwhm_g_arr[i_row, i_col] = fwhm_g_list[idx]
            fwhm_ell_arr[i_row, i_col] = fwhm_ell_list[idx]
            fwhm_pa_arr[i_row, i_col] = fwhm_pa_list[idx]
            ens25_arr[i_row, i_col] = ens25_list[idx]
            ens50_arr[i_row, i_col] = ens50_list[idx]
            ens80_arr[i_row, i_col] = ens80_list[idx]
            enc25_arr[i_row, i_col] = enc25_list[idx]
            enc50_arr[i_row, i_col] = enc50_list[idx]
            enc80_arr[i_row, i_col] = enc80_list[idx]
            ens0025_arr[i_row, i_col] = ens0025_list[idx]
            ens005_arr[i_row, i_col] = ens005_list[idx]
            ens01_arr[i_row, i_col] = ens01_list[idx]
            enc0025_arr[i_row, i_col] = enc0025_list[idx]
            enc005_arr[i_row, i_col] = enc005_list[idx]
            enc01_arr[i_row, i_col] = enc01_list[idx]
            nea_arr[i_row, i_col] = nea_list[idx]
            psf_res_sum_arr[i_row, i_col] = psf_res_sum_list[idx]
            psf_res_std_arr[i_row, i_col] = psf_res_std_list[idx]

        sr_arr = sr_arr * 100
        fwhm_maj_arr = fwhm_maj_arr * 1000
        fwhm_min_arr = fwhm_min_arr * 1000
        fwhm_a_arr = fwhm_a_arr * 1000
        fwhm_g_arr = fwhm_g_arr * 1000
        ens25_arr = ens25_arr * 1000
        ens50_arr = ens50_arr * 1000
        ens80_arr = ens80_arr * 1000
        enc25_arr = enc25_arr * 1000
        enc50_arr = enc50_arr * 1000
        enc80_arr = enc80_arr * 1000
        ens0025_arr = ens0025_arr * 100
        ens005_arr = ens005_arr * 100
        ens01_arr = ens01_arr * 100
        enc0025_arr = enc0025_arr * 100
        enc005_arr = enc005_arr * 100
        enc01_arr = enc01_arr * 100
        psf_res_sum_arr = psf_res_sum_arr * 100
        psf_res_std_arr = psf_res_std_arr * 100

        # Select the field of regard
        regard_r_px = params_girmos.regard_r / \
            (zenith_arr[int((n_side - 1) / 2), 0] -
             zenith_arr[int((n_side - 1) / 2), 1])

        # Generate metrics plots
        plots_path = path.join(folder_in, 'plots', str(ncpa))

        if path.exists(plots_path):
            shutil.rmtree(plots_path)

        makedirs(plots_path)
        print("      - Plotting PSF metrics ...")
        ax_pos = psf.grid_plot(sr_arr, max_range, tick_step, regard_r_px,
                               ("Strehl ratio\n({0} band, {1} nm NCPA " +
                                "RMS)").format(wl_name, ncpa), 'sr', plots_path,
                               range_val=sr_lim, units="%")
        psf.grid_plot(fwhm_maj_arr, max_range, tick_step, regard_r_px,
                      "Major FWHM\n({0} band, {1} nm NCPA RMS)".format(wl_name,
                                                                       ncpa),
                      'fwhm_maj', plots_path, range_val=fwhm_maj_lim,
                      units="mas", color_r=True)
        psf.grid_plot(fwhm_min_arr, max_range, tick_step, regard_r_px,
                      "Minor FWHM\n({0} band, {1} nm NCPA RMS)".format(wl_name,
                                                                       ncpa),
                      'fwhm_min', plots_path, range_val=fwhm_min_lim,
                      units="mas", color_r=True)
        psf.grid_plot(fwhm_a_arr, max_range, tick_step, regard_r_px,
                      ("FWHM arithmetic mean\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'fwhm_a', plots_path,
                      range_val=fwhm_a_lim, units="mas", color_r=True)
        psf.grid_plot(fwhm_g_arr, max_range, tick_step, regard_r_px,
                      ("FWHM geometric mean\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'fwhm_g', plots_path,
                      range_val=fwhm_g_lim, units="mas", color_r=True)
        psf.grid_plot(fwhm_ell_arr, max_range, tick_step, regard_r_px,
                      "FWHM ellipticity\n({0} band, {1} nm NCPA RMS)".format(
                          wl_name, ncpa), 'fwhm_ell', plots_path,
                      range_val=fwhm_ell_lim, color_r=True)
        psf.grid_angle_plot(x_list, y_list, fwhm_pa_list, fwhm_ell_list,
                            max_range, tick_step, params_girmos.regard_r,
                            ell_scale, ax_pos,
                            "FWHM PA\n({0} band, {1} nm NCPA RMS)".format(
                                wl_name, ncpa), 'fwhm_pa', plots_path)
        psf.grid_plot(ens25_arr, max_range, tick_step, regard_r_px,
                      ("Ensquared energy (25%)\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'ens25', plots_path,
                      range_val=ens25_lim, units="mas", color_r=True)
        psf.grid_plot(ens50_arr, max_range, tick_step, regard_r_px,
                      ("Ensquared energy (50%)\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'ens50', plots_path,
                      range_val=ens50_lim, units="mas", color_r=True)
        psf.grid_plot(ens80_arr, max_range, tick_step, regard_r_px,
                      ("Ensquared energy (80%)\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'ens80', plots_path,
                      range_val=ens80_lim, units="mas", color_r=True)
        psf.grid_plot(enc25_arr, max_range, tick_step, regard_r_px,
                      ("Encircled energy (25%)\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'enc25', plots_path,
                      range_val=enc25_lim, units="mas", color_r=True)
        psf.grid_plot(enc50_arr, max_range, tick_step, regard_r_px,
                      ("Encircled energy (50%)\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'enc50', plots_path,
                      range_val=enc50_lim, units="mas", color_r=True)
        psf.grid_plot(enc80_arr, max_range, tick_step, regard_r_px,
                      ("Encircled energy (80%)\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'enc80', plots_path,
                      range_val=enc80_lim, units="mas", color_r=True)
        psf.grid_plot(ens0025_arr, max_range, tick_step, regard_r_px,
                      ("Ensquared energy at 25 mas\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'ens0025', plots_path,
                      range_val=ens0025_lim, units="%")
        psf.grid_plot(ens005_arr, max_range, tick_step, regard_r_px,
                      ("Ensquared energy at 50 mas\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'ens005', plots_path,
                      range_val=ens005_lim, units="%")
        psf.grid_plot(ens01_arr, max_range, tick_step, regard_r_px,
                      ("Ensquared energy at 100 mas\n({0} band, {1} nm " +
                       "NCPA RMS)").format(wl_name, ncpa), 'ens01', plots_path,
                      range_val=ens01_lim, units="%")
        psf.grid_plot(enc0025_arr, max_range, tick_step, regard_r_px,
                      ("Encircled energy at 25 mas\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'enc0025', plots_path,
                      range_val=enc0025_lim, units="%")
        psf.grid_plot(enc005_arr, max_range, tick_step, regard_r_px,
                      ("Encircled energy at 50 mas\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'enc005', plots_path,
                      range_val=enc005_lim, units="%")
        psf.grid_plot(enc01_arr, max_range, tick_step, regard_r_px,
                      ("Encircled energy at 100 mas\n({0} band, {1} nm " +
                       "NCPA RMS)").format(wl_name, ncpa), 'enc01', plots_path,
                      range_val=enc01_lim, units="%")
        psf.grid_plot(nea_arr, max_range, tick_step, regard_r_px,
                      ("Noise-equivalent area\n({0} band, {1} nm NCPA " +
                       "RMS)").format(wl_name, ncpa), 'nea', plots_path,
                      range_val=nea_lim, units="sq \"", color_r=True)
        psf.grid_plot(psf_res_sum_arr, max_range, tick_step, regard_r_px,
                      "PSF residuals\n({0} band, {1} nm NCPA RMS)".format(
                          wl_name, ncpa), 'psf_res_sum', plots_path,
                      range_val=psf_res_sum_lim, units="%", color_r=True)
        psf.grid_plot(psf_res_std_arr, max_range, tick_step, regard_r_px,
                      "PSF residuals STD\n({0} band, {1} nm NCPA RMS)".format(
                          wl_name, ncpa), 'psf_res_std', plots_path,
                      range_val=psf_res_std_lim, units="%", color_r=True)

        # Generate statistics plots
        psf.stats_plot(sr_arr, zenith_arr, params_girmos.regard_r, "SR", '.1f',
                       "Strehl ratio\n({0} band, {1} nm NCPA RMS)".format(
                           wl_name, ncpa), 'stats_sr', plots_path,
                       max_val=(sr_lim[1] * 1.05), units="%")
        psf.stats_plot(fwhm_maj_arr, zenith_arr, params_girmos.regard_r, "FWHM",
                       '.0f',
                       "Major FWHM\n({0} band, {1} nm NCPA RMS)".format(wl_name,
                                                                        ncpa),
                       'stats_fwhm_maj', plots_path,
                       max_val=(fwhm_maj_lim[1] * 1.05), units="mas")
        psf.stats_plot(fwhm_min_arr, zenith_arr, params_girmos.regard_r, "FWHM",
                       '.0f',
                       "Minor FWHM\n({0} band, {1} nm NCPA RMS)".format(wl_name,
                                                                        ncpa),
                       'stats_fwhm_min', plots_path,
                       max_val=(fwhm_min_lim[1] * 1.05), units="mas")
        psf.stats_plot(fwhm_a_arr, zenith_arr, params_girmos.regard_r, "FWHM",
                       '.0f',
                       ("FWHM arithmetic mean\n({0} band, {1} nm NCPA " +
                        "RMS)").format(wl_name, ncpa), 'stats_fwhm_a',
                       plots_path, max_val=(fwhm_a_lim[1] * 1.05), units="mas")
        psf.stats_plot(fwhm_g_arr, zenith_arr, params_girmos.regard_r, "FWHM",
                       '.0f',
                       ("FWHM geometric mean\n({0} band, {1} nm NCPA " +
                        "RMS)").format(wl_name, ncpa), 'stats_fwhm_g',
                       plots_path, max_val=(fwhm_g_lim[1] * 1.05), units="mas")
        psf.stats_plot(fwhm_ell_arr, zenith_arr, params_girmos.regard_r,
                       "Ellipticity", '.3f',
                       "FWHM ellipticity\n({0} band, {1} nm NCPA RMS)".format(
                           wl_name, ncpa), 'stats_fwhm_ell', plots_path,
                       max_val=(fwhm_ell_lim[1] * 1.05))
        psf.stats_plot(ens25_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.0f',
                       ("Ensquared energy (25%)\n({0} band, {1} nm NCPA " +
                        "RMS)").format(wl_name, ncpa), 'stats_ens25',
                       plots_path, max_val=(ens25_lim[1] * 1.05), units="mas")
        psf.stats_plot(ens50_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.0f',
                       ("Ensquared energy (50%)\n({0} band, {1} nm NCPA " +
                        "RMS)").format(wl_name, ncpa), 'stats_ens50',
                       plots_path, max_val=(ens50_lim[1] * 1.05), units="mas")
        psf.stats_plot(ens80_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.0f',
                       ("Ensquared energy (80%)\n({0} band, {1} nm NCPA " +
                        "RMS)").format(wl_name, ncpa), 'stats_ens80',
                       plots_path, max_val=(ens80_lim[1] * 1.05), units="mas")
        psf.stats_plot(enc25_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.0f',
                       ("Encircled energy (25%)\n({0} band, {1} nm NCPA " +
                        "RMS)").format(wl_name, ncpa), 'stats_enc25',
                       plots_path, max_val=(enc25_lim[1] * 1.05), units="mas")
        psf.stats_plot(enc50_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.0f',
                       ("Encircled energy (50%)\n({0} band, {1} nm NCPA " +
                        "RMS)").format(wl_name, ncpa), 'stats_enc50',
                       plots_path, max_val=(enc50_lim[1] * 1.05), units="mas")
        psf.stats_plot(enc80_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.0f',
                       ("Encircled energy (80%)\n({0} band, {1} nm NCPA " +
                        "RMS)").format(wl_name, ncpa), 'stats_enc80',
                       plots_path, max_val=(enc80_lim[1] * 1.05), units="mas")
        psf.stats_plot(ens0025_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.1f',
                       ("Ensquared energy at 25 mas\n({0} band, {1} nm " +
                        "NCPA RMS)").format(wl_name, ncpa), 'stats_ens0025',
                       plots_path, max_val=(ens0025_lim[1] * 1.05), units="%")
        psf.stats_plot(ens005_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.1f',
                       ("Ensquared energy at 50 mas\n({0} band, {1} nm " +
                        "NCPA RMS)").format(wl_name, ncpa), 'stats_ens005',
                       plots_path, max_val=(ens005_lim[1] * 1.05), units="%")
        psf.stats_plot(ens01_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.1f',
                       ("Ensquared energy at 100 mas\n({0} band, {1} nm " +
                        "NCPA RMS)").format(wl_name, ncpa), 'stats_ens01',
                       plots_path, max_val=(ens01_lim[1] * 1.05), units="%")
        psf.stats_plot(enc0025_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.1f',
                       ("Encircled energy at 25 mas\n({0} band, {1} nm " +
                        "NCPA RMS)").format(wl_name, ncpa), 'stats_enc0025',
                       plots_path, max_val=(enc0025_lim[1] * 1.05), units="%")
        psf.stats_plot(enc005_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.1f',
                       ("Encircled energy at 50 mas\n({0} band, {1} nm " +
                        "NCPA RMS)").format(wl_name, ncpa), 'stats_enc005',
                       plots_path, max_val=(enc005_lim[1] * 1.05), units="%")
        psf.stats_plot(enc01_arr, zenith_arr, params_girmos.regard_r, "EE",
                       '.1f',
                       ("Encircled energy at 100 mas\n({0} band, {1} nm " +
                        "NCPA RMS)").format(wl_name, ncpa), 'stats_enc01',
                       plots_path, max_val=(enc01_lim[1] * 1.05), units="%")
        psf.stats_plot(nea_arr, zenith_arr, params_girmos.regard_r, "NEA",
                       '.0f',
                       ("Noise-equivalent area\n({0} band, {1} nm NCPA " +
                        "RMS)").format(wl_name, ncpa), 'stats_nea', plots_path,
                       max_val=(nea_lim[1] * 1.05), units="sq \"")
        psf.stats_plot(psf_res_sum_arr, zenith_arr, params_girmos.regard_r,
                       "PSF residuals", '.1f',
                       "PSF residuals\n({0} band, {1} nm NCPA RMS)".format(
                           wl_name, ncpa), 'stats_psf_res_sum', plots_path,
                       max_val=(psf_res_sum_lim[1] * 1.05), units="%")
        psf.stats_plot(psf_res_std_arr, zenith_arr, params_girmos.regard_r,
                       "PSF residuals STD", '.1f',
                       "PSF residuals STD\n({0} band, {1} nm NCPA RMS)".format(
                           wl_name, ncpa), 'stats_psf_res_std', plots_path,
                       max_val=(psf_res_std_lim[1] * 1.05), units="%")

        # Generate PSF plots
        plots_psfs_path = path.join(plots_path, 'psfs')
        plots_enss_path = path.join(plots_path, 'enss')
        plots_encs_path = path.join(plots_path, 'encs')
        makedirs(plots_psfs_path)
        makedirs(plots_enss_path)
        makedirs(plots_encs_path)
        zoom_px_min = int((psfs['psfs_arr'].shape[0] / 2) -
                          np.ceil(psf_zoom / psfs['ps']))
        zoom_px_max = int((psfs['psfs_arr'].shape[0] / 2) +
                          np.ceil(psf_zoom / psfs['ps']))
        zoom_px_min_over = int((psfs_over['psfs_arr'].shape[0] / 2) -
                               np.ceil(psf_zoom / psfs_over['ps']))
        zoom_px_max_over = int((psfs_over['psfs_arr'].shape[0] / 2) +
                               np.ceil(psf_zoom / psfs_over['ps']))
        psf_color_cut = psfs['psfs_arr'][zoom_px_min: zoom_px_max,
                                         zoom_px_min: zoom_px_max]
        psf_color_cut_over = psfs_over['psfs_arr'][zoom_px_min_over:
                                                   zoom_px_max_over,
                                                   zoom_px_min_over:
                                                   zoom_px_max_over]
        psfs_color_range = [np.min(psf_color_cut[np.where(psf_color_cut > 0)]),
                            np.max(psf_color_cut)]
        psfs_over_color_range = \
            [np.min(psf_color_cut_over[np.where(psf_color_cut_over > 0)]),
             np.max(psf_color_cut_over)]
        psfs_over_res_color = np.max(np.abs(psfs_over_res['psfs_arr']))
        psfs_over_res_color_range = [-psfs_over_res_color, psfs_over_res_color]

        for i_psf in range(n):
            print("\r      - Plotting PSF # {0}/{1} ...".format((i_psf + 1), n),
                  end="")
            img_file = '_x{0:.0f}_y{1:.0f}.png'.format(x_list[i_psf],
                                                       y_list[i_psf])
            psf.psf_plot(psfs['psfs_arr'][:, :, i_psf], psfs['ps'],
                         zoom=psf_zoom, ticks=psf_tick,
                         color_range=psfs_color_range,
                         title=("PSF\n(x={0:.0f}\", y={1:.0f}\", {2} band, " +
                                "{3} nm NCPA RMS)").format(x_list[i_psf],
                                                           y_list[i_psf],
                                                           wl_name, ncpa),
                         save=path.join(plots_psfs_path, ('psf' + img_file)))
            psf.psf_plot(psfs_over['psfs_arr'][:, :, i_psf], psfs_over['ps'],
                         zoom=psf_zoom, ticks=psf_tick,
                         color_range=psfs_over_color_range,
                         title=("Oversampled PSF\n(x={0:.0f}\", y={1:.0f}\", " +
                                "{2} band, {3} nm NCPA RMS)").format(
                             x_list[i_psf], y_list[i_psf], wl_name, ncpa),
                         save=path.join(plots_psfs_path,
                                        ('psf_over' + img_file)))
            psf.psf_plot(psfs_over_res['psfs_arr'][:, :, i_psf],
                         psfs_over_res['ps'],
                         zoom=psf_zoom, ticks=psf_tick, color='zero',
                         color_range=psfs_over_res_color_range,
                         psf_zero=psf_res_zero,
                         title=("Residual oversampled PSF\n(x={0:.0f}\", " +
                                "y={1:.0f}\", {2} band, {3} nm NCPA " +
                                "RMS)").format(x_list[i_psf], y_list[i_psf],
                                               wl_name, ncpa),
                         save=path.join(plots_psfs_path,
                                        ('psf_over_res' + img_file)))
            psf.en_plot(ens_list[i_psf, :], ens_rad_list,
                        ("Ensquared energy\n(x={0:.0f}\", y={1:.0f}\", " +
                         "{2} band, {3} nm NCPA RMS)").format(x_list[i_psf],
                                                              y_list[i_psf],
                                                              wl_name, ncpa),
                        save=path.join(plots_enss_path, ('ens' + img_file)))
            psf.en_plot(enc_list[i_psf, :], enc_rad_list,
                        ("Ensquared energy\n(x={0:.0f}\", y={1:.0f}\", " +
                         "{2} band, {3} nm NCPA RMS)").format(x_list[i_psf],
                                                              y_list[i_psf],
                                                              wl_name, ncpa),
                        save=path.join(plots_encs_path, ('enc' + img_file)))

        print("\n      - Plotting PSF grids ...")
        psf.grid_psf_plot(psfs['psfs_arr'], col_list, row_list, psfs['ps'],
                          side=psf_grid_side, zoom=psf_zoom,
                          title="PSFs\n({0} band, {1} nm NCPA RMS)".format(
                              wl_name, ncpa),
                          save=path.join(plots_psfs_path, 'psf_grid.png'))
        psf.grid_psf_plot(psfs_over['psfs_arr'], col_list, row_list,
                          psfs_over['ps'], side=psf_grid_side, zoom=psf_zoom,
                          title=("Oversampled PSFs\n({0} band, {1} nm NCPA " +
                                 "RMS)").format(wl_name, ncpa),
                          save=path.join(plots_psfs_path, 'psf_over_grid.png'))
        psf.grid_psf_plot(psfs_over_res['psfs_arr'], col_list, row_list,
                          psfs_over_res['ps'], side=psf_grid_side,
                          zoom=psf_zoom, color='zero', psf_zero=psf_res_zero,
                          title=("Residual oversampled PSFs\n({0} band, {1} " +
                                 "nm NCPA RMS)").format(wl_name, ncpa),
                          save=path.join(plots_psfs_path,
                                         'psf_over_res_grid.png'))

        # Generate meta-metrics
        print("      - Recording PSF meta-metrics ...")
        idx_meta = np.where(zenith_arr <= params_girmos.regard_r)
        sr_min = np.min(sr_arr[idx_meta])
        fwhm_maj_min = np.min(fwhm_maj_arr[idx_meta])
        fwhm_min_min = np.min(fwhm_min_arr[idx_meta])
        fwhm_a_min = np.min(fwhm_a_arr[idx_meta])
        fwhm_g_min = np.min(fwhm_g_arr[idx_meta])
        fwhm_ell_min = np.min(fwhm_ell_arr[idx_meta])
        ens25_min = np.min(ens25_arr[idx_meta])
        ens50_min = np.min(ens50_arr[idx_meta])
        ens80_min = np.min(ens80_arr[idx_meta])
        enc25_min = np.min(enc25_arr[idx_meta])
        enc50_min = np.min(enc50_arr[idx_meta])
        enc80_min = np.min(enc80_arr[idx_meta])
        ens0025_min = np.min(ens0025_arr[idx_meta])
        ens005_min = np.min(ens005_arr[idx_meta])
        ens01_min = np.min(ens01_arr[idx_meta])
        enc0025_min = np.min(enc0025_arr[idx_meta])
        enc005_min = np.min(enc005_arr[idx_meta])
        enc01_min = np.min(enc01_arr[idx_meta])
        nea_min = np.min(nea_arr[idx_meta])
        psf_res_std_min = np.min(psf_res_std_arr[idx_meta])
        psf_res_sum_min = np.min(psf_res_sum_arr[idx_meta])
        sr_max = np.max(sr_arr[idx_meta])
        fwhm_maj_max = np.max(fwhm_maj_arr[idx_meta])
        fwhm_min_max = np.max(fwhm_min_arr[idx_meta])
        fwhm_a_max = np.max(fwhm_a_arr[idx_meta])
        fwhm_g_max = np.max(fwhm_g_arr[idx_meta])
        fwhm_ell_max = np.max(fwhm_ell_arr[idx_meta])
        ens25_max = np.max(ens25_arr[idx_meta])
        ens50_max = np.max(ens50_arr[idx_meta])
        ens80_max = np.max(ens80_arr[idx_meta])
        enc25_max = np.max(enc25_arr[idx_meta])
        enc50_max = np.max(enc50_arr[idx_meta])
        enc80_max = np.max(enc80_arr[idx_meta])
        ens0025_max = np.max(ens0025_arr[idx_meta])
        ens005_max = np.max(ens005_arr[idx_meta])
        ens01_max = np.max(ens01_arr[idx_meta])
        enc0025_max = np.max(enc0025_arr[idx_meta])
        enc005_max = np.max(enc005_arr[idx_meta])
        enc01_max = np.max(enc01_arr[idx_meta])
        nea_max = np.max(nea_arr[idx_meta])
        psf_res_std_max = np.max(psf_res_std_arr[idx_meta])
        psf_res_sum_max = np.max(psf_res_sum_arr[idx_meta])
        sr_mean = np.mean(sr_arr[idx_meta])
        fwhm_maj_mean = np.mean(fwhm_maj_arr[idx_meta])
        fwhm_min_mean = np.mean(fwhm_min_arr[idx_meta])
        fwhm_a_mean = np.mean(fwhm_a_arr[idx_meta])
        fwhm_g_mean = np.mean(fwhm_g_arr[idx_meta])
        fwhm_ell_mean = np.mean(fwhm_ell_arr[idx_meta])
        ens25_mean = np.mean(ens25_arr[idx_meta])
        ens50_mean = np.mean(ens50_arr[idx_meta])
        ens80_mean = np.mean(ens80_arr[idx_meta])
        enc25_mean = np.mean(enc25_arr[idx_meta])
        enc50_mean = np.mean(enc50_arr[idx_meta])
        enc80_mean = np.mean(enc80_arr[idx_meta])
        ens0025_mean = np.mean(ens0025_arr[idx_meta])
        ens005_mean = np.mean(ens005_arr[idx_meta])
        ens01_mean = np.mean(ens01_arr[idx_meta])
        enc0025_mean = np.mean(enc0025_arr[idx_meta])
        enc005_mean = np.mean(enc005_arr[idx_meta])
        enc01_mean = np.mean(enc01_arr[idx_meta])
        nea_mean = np.mean(nea_arr[idx_meta])
        psf_res_std_mean = np.mean(psf_res_std_arr[idx_meta])
        psf_res_sum_mean = np.mean(psf_res_sum_arr[idx_meta])
        sr_std = np.std(sr_arr[idx_meta])
        fwhm_maj_std = np.std(fwhm_maj_arr[idx_meta])
        fwhm_min_std = np.std(fwhm_min_arr[idx_meta])
        fwhm_a_std = np.std(fwhm_a_arr[idx_meta])
        fwhm_g_std = np.std(fwhm_g_arr[idx_meta])
        fwhm_ell_std = np.std(fwhm_ell_arr[idx_meta])
        ens25_std = np.std(ens25_arr[idx_meta])
        ens50_std = np.std(ens50_arr[idx_meta])
        ens80_std = np.std(ens80_arr[idx_meta])
        enc25_std = np.std(enc25_arr[idx_meta])
        enc50_std = np.std(enc50_arr[idx_meta])
        enc80_std = np.std(enc80_arr[idx_meta])
        ens0025_std = np.std(ens0025_arr[idx_meta])
        ens005_std = np.std(ens005_arr[idx_meta])
        ens01_std = np.std(ens01_arr[idx_meta])
        enc0025_std = np.std(enc0025_arr[idx_meta])
        enc005_std = np.std(enc005_arr[idx_meta])
        enc01_std = np.std(enc01_arr[idx_meta])
        nea_std = np.std(nea_arr[idx_meta])
        psf_res_sum_std = np.std(psf_res_sum_arr[idx_meta])
        psf_res_std_std = np.std(psf_res_std_arr[idx_meta])
        sr_cv = sr_std / sr_mean
        fwhm_maj_cv = fwhm_maj_std / fwhm_maj_mean
        fwhm_min_cv = fwhm_min_std / fwhm_min_mean
        fwhm_a_cv = fwhm_a_std / fwhm_a_mean
        fwhm_g_cv = fwhm_g_std / fwhm_g_mean
        fwhm_ell_cv = fwhm_ell_std / fwhm_ell_mean
        ens25_cv = ens25_std / ens25_mean
        ens50_cv = ens50_std / ens50_mean
        ens80_cv = ens80_std / ens80_mean
        enc25_cv = enc25_std / enc25_mean
        enc50_cv = enc50_std / enc50_mean
        enc80_cv = enc80_std / enc80_mean
        ens0025_cv = ens0025_std / ens0025_mean
        ens005_cv = ens005_std / ens005_mean
        ens01_cv = ens01_std / ens01_mean
        enc0025_cv = enc0025_std / enc0025_mean
        enc005_cv = enc005_std / enc005_mean
        enc01_cv = enc01_std / enc01_mean
        nea_cv = nea_std / nea_mean
        psf_res_sum_cv = psf_res_sum_std / psf_res_sum_mean
        psf_res_std_cv = psf_res_std_std / psf_res_std_mean
        header_str = "\n{0} band, {1} nm NCPA RMS\n\n".format(wl_name, ncpa)
        sr_str = ("SR\n   Min: {0:.1f} %\n   Max: {1:.1f} %\n   " +
                  "Mean: {2:.1f} %\n   STD: {3:.1f} %\n   " +
                  "CV: {4:.3f}\n\n").format(sr_min, sr_max, sr_mean, sr_std,
                                            sr_cv)
        fwhm_maj_str = ("Major FWHM\n   Min: {0:.1f} mas\n   " +
                        "Max: {1:.1f} mas\n   Mean: {2:.1f} mas\n   " +
                        "STD: {3:.1f} mas\n   " +
                        "CV: {4:.3f}\n\n").format(fwhm_maj_min, fwhm_maj_max,
                                                  fwhm_maj_mean, fwhm_maj_std,
                                                  fwhm_maj_cv)
        fwhm_min_str = ("Minor FWHM\n   Min: {0:.1f} mas\n   " +
                        "Max: {1:.1f} mas\n   Mean: {2:.1f} mas\n   " +
                        "STD: {3:.1f} mas\n   " +
                        "CV: {4:.3f}\n\n").format(fwhm_min_min, fwhm_min_max,
                                                  fwhm_min_mean, fwhm_min_std,
                                                  fwhm_min_cv)
        fwhm_a_str = ("FWHM arithmetic mean\n   Min: {0:.1f} mas\n   " +
                      "Max: {1:.1f} mas\n   Mean: {2:.1f} mas\n   " +
                      "STD: {3:.1f} mas\n   " +
                      "CV: {4:.3f}\n\n").format(fwhm_a_min, fwhm_a_max,
                                                fwhm_a_mean, fwhm_a_std,
                                                fwhm_a_cv)
        fwhm_g_str = ("FWHM geometric mean\n   Min: {0:.1f} mas\n   " +
                      "Max: {1:.1f} mas\n   Mean: {2:.1f} mas\n   " +
                      "STD: {3:.1f} mas\n   " +
                      "CV: {4:.3f}\n\n").format(fwhm_g_min, fwhm_g_max,
                                                fwhm_g_mean, fwhm_g_std,
                                                fwhm_g_cv)
        fwhm_ell_str = ("FWHM ellipticity\n   Min: {0:.3f}\n   " +
                        "Max: {1:.3f}\n   Mean: {2:.3f}\n   STD: {3:.3f}\n   " +
                        "CV: {4:.3f}\n\n").format(fwhm_ell_min, fwhm_ell_max,
                                                  fwhm_ell_mean, fwhm_ell_std,
                                                  fwhm_ell_cv)
        ens25_str = ("Ensquared energy (25%)\n   Min: {0:.1f} mas\n   " +
                     "Max: {1:.1f} mas\n   Mean: {2:.1f} mas\n   " +
                     "STD: {3:.1f} mas\n   CV: {4:.3f}\n\n").format(ens25_min,
                                                                    ens25_max,
                                                                    ens25_mean,
                                                                    ens25_std,
                                                                    ens25_cv)
        ens50_str = ("Ensquared energy (50%)\n   Min: {0:.1f} mas\n   " +
                     "Max: {1:.1f} mas\n   Mean: {2:.1f} mas\n   " +
                     "STD: {3:.1f} mas\n   CV: {4:.3f}\n\n").format(ens50_min,
                                                                    ens50_max,
                                                                    ens50_mean,
                                                                    ens50_std,
                                                                    ens50_cv)
        ens80_str = ("Ensquared energy (80%)\n   Min: {0:.1f} mas\n   " +
                     "Max: {1:.1f} mas\n   Mean: {2:.1f} mas\n   " +
                     "STD: {3:.1f} mas\n   CV: {4:.3f}\n\n").format(ens80_min,
                                                                    ens80_max,
                                                                    ens80_mean,
                                                                    ens80_std,
                                                                    ens80_cv)
        enc25_str = ("Encircled energy (25%)\n   Min: {0:.1f} mas\n   " +
                     "Max: {1:.1f} mas\n   Mean: {2:.1f} mas\n   " +
                     "STD: {3:.1f} mas\n   CV: {4:.3f}\n\n").format(enc25_min,
                                                                    enc25_max,
                                                                    enc25_mean,
                                                                    enc25_std,
                                                                    enc25_cv)
        enc50_str = ("Encircled energy (50%)\n   Min: {0:.1f} mas\n   " +
                     "Max: {1:.1f} mas\n   Mean: {2:.1f} mas\n   " +
                     "STD: {3:.1f} mas\n   CV: {4:.3f}\n\n").format(enc50_min,
                                                                    enc50_max,
                                                                    enc50_mean,
                                                                    enc50_std,
                                                                    enc50_cv)
        enc80_str = ("Encircled energy (80%)\n   Min: {0:.1f} mas\n   " +
                     "Max: {1:.1f} mas\n   Mean: {2:.1f} mas\n   " +
                     "STD: {3:.1f} mas\n   CV: {4:.3f}\n\n").format(enc80_min,
                                                                    enc80_max,
                                                                    enc80_mean,
                                                                    enc80_std,
                                                                    enc80_cv)
        ens0025_str = ("Ensquared energy at 25 mas\n   Min: {0:.1f} %\n   " +
                       "Max: {1:.1f} %\n   Mean: {2:.1f} %\n   " +
                       "STD: {3:.1f} %\n   "
                       "CV: {4:.3f}\n\n").format(ens0025_min, ens0025_max,
                                                 ens0025_mean, ens0025_std,
                                                 ens0025_cv)
        ens005_str = ("Ensquared energy at 50 mas\n   Min: {0:.1f} %\n   " +
                      "Max: {1:.1f} %\n   Mean: {2:.1f} %\n   " +
                      "STD: {3:.1f} %\n   CV: {4:.3f}\n\n").format(ens005_min,
                                                                   ens005_max,
                                                                   ens005_mean,
                                                                   ens005_std,
                                                                   ens005_cv)
        ens01_str = ("Ensquared energy at 100 mas\n   Min: {0:.1f} %\n   " +
                     "Max: {1:.1f} %\n   Mean: {2:.1f} %\n   " +
                     "STD: {3:.1f} %\n   CV: {4:.3f}\n\n").format(ens01_min,
                                                                  ens01_max,
                                                                  ens01_mean,
                                                                  ens01_std,
                                                                  ens01_cv)
        enc0025_str = ("Encircled energy at 25 mas\n   Min: {0:.1f} %\n   " +
                       "Max: {1:.1f} %\n   Mean: {2:.1f} %\n   " +
                       "STD: {3:.1f} %\n   " +
                       "CV: {4:.3f}\n\n").format(enc0025_min, enc0025_max,
                                                 enc0025_mean, enc0025_std,
                                                 enc0025_cv)
        enc005_str = ("Encircled energy at 50 mas\n   Min: {0:.1f} %\n   " +
                      "Max: {1:.1f} %\n   Mean: {2:.1f} %\n   " +
                      "STD: {3:.1f} %\n   CV: {4:.3f}\n\n").format(enc005_min,
                                                                   enc005_max,
                                                                   enc005_mean,
                                                                   enc005_std,
                                                                   enc005_cv)
        enc01_str = ("Encircled energy at 100 mas\n   Min: {0:.1f} %\n   " +
                     "Max: {1:.1f} %\n   Mean: {2:.1f} %\n   " +
                     "STD: {3:.1f} %\n   CV: {4:.3f}\n\n").format(enc01_min,
                                                                  enc01_max,
                                                                  enc01_mean,
                                                                  enc01_std,
                                                                  enc01_cv)
        nea_str = ("Noise-equivalent area\n   Min: {0:.3f} sq \"\n   " +
                   "Max: {1:.3f} sq \"\n   Mean: {2:.3f} sq \"\n   " +
                   "STD: {3:.3f} sq \"\n   CV: {4:.3f}\n\n").format(nea_min,
                                                                    nea_max,
                                                                    nea_mean,
                                                                    nea_std,
                                                                    nea_cv)
        psf_res_sum_str = ("PSF residuals\n   Min: {0:.1f} %\n   " +
                           "Max: {1:.1f} %\n   Mean: {2:.1f} %\n   " +
                           "STD: {3:.1f} %\n   " +
                           "CV: {4:.3f}\n\n").format(psf_res_sum_min,
                                                     psf_res_sum_max,
                                                     psf_res_sum_mean,
                                                     psf_res_sum_std,
                                                     psf_res_sum_cv)
        psf_res_std_str = ("PSF residuals STD\n   Min: {0:.1f} %\n   " +
                           "Max: {1:.1f} %\n   Mean: {2:.1f} %\n   " +
                           "STD: {3:.1f} %\n   " +
                           "CV: {4:.3f}\n").format(psf_res_std_min,
                                                   psf_res_std_max,
                                                   psf_res_std_mean,
                                                   psf_res_std_std,
                                                   psf_res_std_cv)
        plots_meta_path = path.join(plots_path, 'meta')
        makedirs(plots_meta_path)
        meta_file = open(path.join(plots_meta_path, 'metametrics.txt'), 'w')
        meta_file.write(header_str)
        meta_file.write(sr_str)
        meta_file.write(fwhm_maj_str)
        meta_file.write(fwhm_min_str)
        meta_file.write(fwhm_a_str)
        meta_file.write(fwhm_g_str)
        meta_file.write(fwhm_ell_str)
        meta_file.write(ens25_str)
        meta_file.write(ens50_str)
        meta_file.write(ens80_str)
        meta_file.write(enc25_str)
        meta_file.write(enc50_str)
        meta_file.write(enc80_str)
        meta_file.write(ens0025_str)
        meta_file.write(ens005_str)
        meta_file.write(ens01_str)
        meta_file.write(enc0025_str)
        meta_file.write(enc005_str)
        meta_file.write(enc01_str)
        meta_file.write(nea_str)
        meta_file.write(psf_res_sum_str)
        meta_file.write(psf_res_std_str)
        meta_file.close()

end_time = time.time()
elapsed_time = end_time - start_time
elapsed_h = int(np.floor(elapsed_time / (60 * 60)))
elapsed_m = int(np.floor((elapsed_time / 60) - (elapsed_h * 60)))
elapsed_s = int(np.floor(elapsed_time - (elapsed_m * 60) -
                         (elapsed_h * 60 * 60)))
print("Time elapsed: {0:02d}h:{1:02d}m:{2:02d}s".format(elapsed_h, elapsed_m,
                                                        elapsed_s))
print("Program terminated\n")
