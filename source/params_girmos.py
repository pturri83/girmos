"""List of fixed parameters for GIRMOS.

References:
    [ref1] Cohen et al., 2013
"""


import numpy as np


# Fixed parameters (math)
rad_to_arcsec = 180 * 60 * 60 / np.pi  # Conversion from radians to arcseconds

# Fixed parameters (instrument)
d = 8  # Telescope diameter (m)
d_o = 1.12  # Telescope central obstruction diameter (m)
ps = 0.02  # Pixel scale ["/px]
act = 17  # Number of actuators across the GIRMOS DM diameter
regard_r = 60  # Field of regard radius (")

# Fixed parameters (physics)
wl_names = ['J', 'H', 'Ks']  # 2MASS filters
wls = [1.235, 1.662, 2.159]  # Isophotal wavelengths (um) [ref1]
