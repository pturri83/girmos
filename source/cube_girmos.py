"""Functions to handle GIRMOS data cubes.
"""


import warnings

from scipy import ndimage


def convolve(image_in, psf_in):
    """Convolve a high-resolution image with a GIRMOS PSF.

    Parameters:
        :param image_in: Image to convolve
        :type image_in: numpy.ndarray [x, y] (float)
        :param psf_in: PSF
        :type psf_in: numpy.ndarray [m, n] (float)

    Returns:
        :return image_out: Convolved image
        :rtype image_out: numpy.ndarray [x, y] (float)
    """

    # Convolve the image
    print("Convolving image with PSF ...")
    image_out = ndimage.convolve(image_in, psf_in)

    return image_out


def scale(image_in, ps_in, ps_out):
    """Scale an image changing the pixel scale.

    Parameters:
        :param image_in: Image to scale
        :type image_in: numpy.ndarray [y1, x2] (float)
        :param ps_in: Input pixel scale ("/px)
        :type ps_in: float
        :param ps_out: Output pixel scale ("/px)
        :type ps_out: float

    Returns:
        :return image_out: Scaled image
        :rtype image_out: numpy.ndarray [y2, x2] (float)
    """

    # Scale the image
    print("Scaling image ...")
    warnings.simplefilter('ignore')
    image_out = ndimage.zoom(image_in, (ps_in / ps_out))
    warnings.simplefilter('default')

    return image_out
