"""Functions to handle IllustrisTNG simulations.

References:
    [ref1] Pillepich et al., 2019
"""


import numpy as np


def h_alpha(sfr, sfr_coord_1, sfr_coord_2, bin_s):
    """Simulate a H_alpha map from the SFR of gas particles.

    Parameters:
        :param sfr: SFR of the gas particles (M_Sun/yr)
        :type sfr: numpy.ndarray [n] (float)
        :param sfr_coord_1: First coordinate of the gas particles (")
        :type sfr_coord_1: numpy.ndarray [n] (float)
        :param sfr_coord_2: Second coordinate of the gas particles (")
        :type sfr_coord_2: numpy.ndarray [n] (float)
        :param bin_s: Bin size (")
        :type bin_s: float

    Returns:
        :return map_halpha: H_alpha luminosity map (erg/s)
        :rtype map_halpha: numpy.ndarray [x, y] (float)
        :return map_part: Map of the number of gas particles
        :rtype map_part: numpy.ndarray [x, y] (float)
    """

    # Calculate the maps
    print("Simulating H_alpha ...")
    bins1 = np.arange(np.min(sfr_coord_1), np.max(sfr_coord_1), bin_s)
    bins2 = np.arange(np.min(sfr_coord_2), np.max(sfr_coord_2), bin_s)
    map_sfr, _, _ = np.histogram2d(sfr_coord_1, sfr_coord_2,
                                   bins=[bins1, bins2], weights=sfr,
                                   density=False)
    map_part, _, _ = np.histogram2d(sfr_coord_1, sfr_coord_2,
                                    bins=[bins1, bins2])
    map_halpha = map_sfr / 7.9e-42  # erg/s [ref1(1)]

    return map_halpha, map_part
