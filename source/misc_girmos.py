"""Miscellaneous functions for GIRMOS analysis.
"""


import numpy as np


def ticks_scale(img_shape, ps, ticks):
    """Convert axes' ticks of an image to ", with the origin (0, 0) at the
    center of the image.

    Parameters:
        :param img_shape: Shape of the image (px)
        :type img_shape: tuple [2] (int)
        :param ps: Pixel scale
        :type ps: float
        :param ticks: Distance of the axes' ticks (")
        :type ticks: float

    Returns:
        :return x_ticks_orig: Positions of the x ticks (px)
        :rtype x_ticks_orig: numpy.ndarray [n] (float)
        :return y_ticks_orig: Positions of the y ticks (px)
        :rtype y_ticks_orig: numpy.ndarray [n] (float)
        :return x_ticks_str: Labels of the x ticks (")
        :rtype x_ticks_str: numpy.ndarray [n] (str)
        :return y_ticks_str: Labels of the y ticks (")
        :rtype y_ticks_str: numpy.ndarray [n] (str)
    """

    # Calculate ticks
    xy_px = np.array(img_shape)
    xy_ticks_max = np.floor((xy_px * ps) / (2 * ticks)) * ticks
    x_ticks = np.arange(-xy_ticks_max[1], (xy_ticks_max[1] + ticks), ticks)
    y_ticks = np.arange(-xy_ticks_max[0], (xy_ticks_max[0] + ticks), ticks)
    x_ticks_orig = (x_ticks / ps) + ((xy_px[1] - 1) / 2)
    y_ticks_orig = (y_ticks / ps) + ((xy_px[0] - 1) / 2)
    x_ticks_str = [str(i) for i in np.around(x_ticks, decimals=10)]
    y_ticks_str = [str(i) for i in np.around(y_ticks, decimals=10)]

    return x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str


def zern_sum(side, n, coeff):
    """Calculate the wavefront on a circular pupil with multiple Zernike
    polynomials. The polynomials are normalized by STD.

    Parameters:
        :param side: Pupil side (px)
        :type side: int
        :param n: Maximum Noll index
        :type n: int
        :param coeff: Coefficients of the Zernike polynomials
        :type coeff: numpy.ndarray [n] (float)

    Returns:
        :return array: Pupil array
        :rtype array: numpy.ndarray [side, side] (float)
    """

    # Calculate the pupil
    array = np.zeros([side, side])
    idx = rad_az(n)

    for i_zern in range(len(coeff)):
        zern = zern_single(side, idx[i_zern][0], idx[i_zern][1])
        array += coeff[i_zern] * zern.astype(np.float64)

    return array


def zern_single(side, n, m):
    """Calculate the wavefront on a circular pupil with a single Zernike
    polynomial. The polynomial is normalized by STD.

    Parameters:
        :param side: Pupil side (px)
        :type side: int
        :param n: Radial Zernike index
        :type n: int
        :param m: Azimuthal Zernike index
        :type m: int

    Returns:
        :return array: Pupil array
        :rtype array: numpy.ndarray [side, side] (float)
    """

    # Calculate polar coordinates
    y_mesh, x_mesh = np.indices([side, side])
    rad = (side - 1) / 2
    rho = np.hypot((y_mesh - rad), (x_mesh - rad)) / rad
    phi = np.arctan2((y_mesh - rad), (x_mesh - rad))
    mask = np.where(rho > 1)

    # Calculate Zernike polynomial
    norm = np.sqrt(2 * (n + 1) / (1 + (m == 0)))
    zern_rad = np.zeros([side, side], dtype=object)

    for k in range(int(((n - np.abs(m)) / 2) + 1)):
        zern_rad += (rho ** (n - (2 * k))) * ((-1) ** k) * \
                    np.math.factorial(n - k) / \
                    (np.math.factorial(k) *
                     np.math.factorial(((n + np.abs(m)) / 2) - k) *
                     np.math.factorial(int((n - m) / 2) - k))

    if m >= 0:
        zern_az = np.cos(m * phi)
    else:
        zern_az = np.sin(-m * phi)

    array = norm * zern_rad * zern_az
    array[mask] = 0

    return array


def rad_az(n):
    """Return the radial and azimuthal indices of the Zernike polynomial up to
    a Noll index (included).

    Parameters:
        :param n: Maximum Noll index
        :type n: int

    Returns:
        :return nm: Zernike indeces [radial, azimuthal]
        :rtype nm: numpy.ndarray [n, 2] (int)
    """

    # Convert index
    nm = np.zeros([n, 2], dtype=int)

    for i_zern in range(n):
        nm[i_zern] = noll_to_zern(i_zern + 1)

    return nm


def noll_to_zern(j):
    """Convert the index of a Zernike polynomial from Noll notazion to Zernike.

    Parameters:
        :param j: Noll index
        :type j: int

    Returns:
        :return nm: Zernike index [radial, azimuthal]
        :rtype nm: list [2] (int)
    """

    # Convert index
    n = 0
    k = j - 1

    while k > n:
        n += 1
        k -= n

    m = ((-1) ** j) * ((n % 2) + 2 * int((k + ((n + 1) % 2)) / 2))
    nm = [n, m]

    return nm
